CREATE DATABASE  IF NOT EXISTS `comp90018app` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `comp90018app`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win32 (AMD64)
--
-- Host: jackrui.uicp.hk    Database: comp90018app
-- ------------------------------------------------------
-- Server version	5.6.27-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `geo_info`
--

DROP TABLE IF EXISTS `geo_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `geo_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(45) NOT NULL,
  `lng` double NOT NULL,
  `lat` double NOT NULL,
  `last_update` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `uid_UNIQUE` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `geo_info`
--

LOCK TABLES `geo_info` WRITE;
/*!40000 ALTER TABLE `geo_info` DISABLE KEYS */;
INSERT INTO `geo_info` VALUES (1,'aa@al4b',145.01009666666667,-37.73959833333333,''),(7,'bb@bvxw',145.010125,-37.73963833333333,''),(8,'a@aaz4t',-122.08400000000002,37.421998333333335,'');
/*!40000 ALTER TABLE `geo_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `msg_cache`
--

DROP TABLE IF EXISTS `msg_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `msg_cache` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` varchar(20) NOT NULL,
  `receiver` varchar(20) NOT NULL,
  `send_time` datetime NOT NULL,
  `msg_content` text,
  PRIMARY KEY (`id`),
  KEY `fk_sender_idx` (`sender`),
  KEY `fk_receiver_idx` (`receiver`),
  CONSTRAINT `fk_receiver` FOREIGN KEY (`receiver`) REFERENCES `users` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sender` FOREIGN KEY (`sender`) REFERENCES `users` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `msg_cache`
--

LOCK TABLES `msg_cache` WRITE;
/*!40000 ALTER TABLE `msg_cache` DISABLE KEYS */;
INSERT INTO `msg_cache` VALUES (1,'1','1','2016-10-08 09:28:09','{\"msgLife\":20,\"msgStr\":\"haha simida\",\"msgType\":\"TEXT\",\"command\":\"CHAT\",\"receiver\":\"1\",\"sender\":\"1\"}'),(17,'bb@bvxw','aa@al4b','2016-10-17 16:52:25',''),(18,'bb@bvxw','aa@al4b','2016-10-17 16:53:06',''),(19,'bb@bvxw','aa@al4b','2016-10-17 16:57:23','');
/*!40000 ALTER TABLE `msg_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_relation`
--

DROP TABLE IF EXISTS `user_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(20) NOT NULL,
  `friend_uid` varchar(20) NOT NULL,
  `friend_remark` varchar(45) DEFAULT NULL,
  `relation_status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `uid_idx` (`uid`),
  KEY `firend_uid_idx` (`friend_uid`),
  CONSTRAINT `firend_uid` FOREIGN KEY (`friend_uid`) REFERENCES `users` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `uid` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_relation`
--

LOCK TABLES `user_relation` WRITE;
/*!40000 ALTER TABLE `user_relation` DISABLE KEYS */;
INSERT INTO `user_relation` VALUES (2,'aa@al4b','bb@bvxw','',0),(3,'bb@bvxw','aa@al4b','',0),(4,'aa@al4b','aa@al4b','',0),(6,'a@aaz4t','bb@bvxw','',0),(7,'a@aaz4t','aa@al4b','',1),(8,'bb@bvxw','a@aaz4t','',0),(9,'bb@bvxw','bb@bvxw','',0);
/*!40000 ALTER TABLE `user_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `uid` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pwd` char(40) NOT NULL,
  `nick` varchar(45) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `token` text,
  `login_flag` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `user_uid_UNIQUE` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('1','2','3','4',NULL,'',0),('a@aaz4t','a@aa','5597a079b7806f29e5af9881fc4d82c775b9926f','alvin','123456','c-i5jtFRj8w:APA91bEWZ2aF6eMpdKF_1lrbf1lt1y4LPmOj1_a78KUyfHiQHk1BqqTh1-Src2BCeqUySIjsJK4lHPTetMS20-kJnoyAiTqULmsEpD7Zbo15QuBdUC5l_TdLVXGguveU8eKp-GT9tuLx',1),('aa@al4b','aa@a','da471912409b91d80047025ab29fb02d2a9fc88a','ACE',NULL,'c-i5jtFRj8w:APA91bEWZ2aF6eMpdKF_1lrbf1lt1y4LPmOj1_a78KUyfHiQHk1BqqTh1-Src2BCeqUySIjsJK4lHPTetMS20-kJnoyAiTqULmsEpD7Zbo15QuBdUC5l_TdLVXGguveU8eKp-GT9tuLx',1),('alvi1k1','alvin@com','cc9312d4ad629db43c2b7718a3c906742928c15c','Alvin','12345678910','c-i5jtFRj8w:APA91bEWZ2aF6eMpdKF_1lrbf1lt1y4LPmOj1_a78KUyfHiQHk1BqqTh1-Src2BCeqUySIjsJK4lHPTetMS20-kJnoyAiTqULmsEpD7Zbo15QuBdUC5l_TdLVXGguveU8eKp-GT9tuLx',1),('bb@bvxw','bb@b','8d413a82e045d950bb04964e0e14204734bc189b','rex','0413173278','dnHM2iFYTb0:APA91bG6eAcorNeGclVkVQ95cIKpSD6FmOomPERuDqnypELPrd-a8D2ZW57M7MAe3Fxb0bVJ5dwjqByu0LBGT_ot5n6a4YU0DZG3eZ5yQyW57esIwbC4-uimSka7Rqx5t8ViF0Efqm9V',1),('cc@c7g7','cc@c','40095f2c8de813ab7502dfc06d7c7169e90dd529','Alvin','0413173278','',0),('dd@dhja','dd@d','b614401050ac2583c3354b849f6da316ca523ab1','dd','','dDJKuruXQIc:APA91bHNn6KpJq7jyK2fll_GBWDXktBaIwTm8sSRPgs824NUN_-LAZrZSA-JxXvpjiaEt2csrP8vOzoyJYA35CJp7hE18szcoHQE5MmOsIuNL5OwmeE2i0mX72W9kv0RBtiR2Cudv59a',1),('ee@et5j','ee@e','66be33504627e512f24773de629f51f6386f4c5c','ee','','dDJKuruXQIc:APA91bHNn6KpJq7jyK2fll_GBWDXktBaIwTm8sSRPgs824NUN_-LAZrZSA-JxXvpjiaEt2csrP8vOzoyJYA35CJp7hE18szcoHQE5MmOsIuNL5OwmeE2i0mX72W9kv0RBtiR2Cudv59a',1),('ff@ff3t','ff@f','122e32c603fb433da5e39af83307e03b9ea29f43','fff','','fhhv-DfY8VA:APA91bHxb-NKcBLaegWbg9GAYEc2QieD3bXUclgEP3Cq-9jZu1CRqD9hxDz6uxs4zv0S8jTtd4lwMZxszujM-FVYL-u7ElVHrgQ2EWjA4Ncwe6SEUaQ314u5IexOVFn_nSuEiQpUXv9T',1),('gg@gsjn','gg@g','b4268d5b3211f2e93cc77059ce425a08f5af1230','ggg','','cYUMdE-ovyo:APA91bERewe5IMfMJ62ZmAehAz0LoDxy40iY2pi-gTiV0f0yOhLLEG2OZqPgRzxD_WCi64r5j2C45yMm3trSDud3xwf4zZiCuX9W7w76sR4ink9p-PXl0a1eLQFLhGOz8rvaZxYEOkap',1),('hh@hyen','hh@h','a1cccc2fe26673d492f0679c9b4a536c090a5ff6','hhh','','f8t5mLleTBM:APA91bG-hVBI9sYQ14__I7t39O0cZ8Gj_kxuivFStEPk8SJvnuYjRrbVc9RzS4g-1G89LVD0UpvgT_DYRQr3MfreY81ZZc0lenlw3Fb5ZhxK-0SgGv3xFrgZUQXJN40pb4EdOfaJD5Fm',1),('xxxx8Ad','xxxxxxx','xx','',NULL,'',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-18  4:32:19
