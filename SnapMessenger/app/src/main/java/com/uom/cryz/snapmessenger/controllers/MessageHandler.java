package com.uom.cryz.snapmessenger.controllers;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.squareup.otto.Bus;
import com.uom.cryz.snapmessenger.MainActivity;
import com.uom.cryz.snapmessenger.fbservices.MyFirebaseStorageManager;
import com.uom.cryz.snapmessenger.models.Envelop;
import com.uom.cryz.snapmessenger.models.messages.ChatMsg;
import com.uom.cryz.snapmessenger.models.messages.FriendAckMsg;
import com.uom.cryz.snapmessenger.models.messages.FriendReqMsg;
import com.uom.cryz.snapmessenger.models.responses.FriendGetResponse;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

/**
 * Created by ruijiankun on 6/10/2016.
 */
public class MessageHandler {
    private Bus mBus;

    private static MessageHandler msgHandler = null;
    private static final String TAG = MessageHandler.class.getSimpleName();

    public static MessageHandler getInstance() {
        if (msgHandler == null) {
            msgHandler = new MessageHandler();
        }
        return msgHandler;
    }

    public MessageHandler() {
        this.mBus = BusProvider.getInstance();

    }

    MyVolley.VolleyCallback friendReqCallback = new MyVolley.VolleyCallback() {
        @Override
        public void onSuccess(Object result) {
            FriendGetResponse resp = (FriendGetResponse) result;
            if (resp.success) {
                DataController.getInstance().saveFriendReq(MainActivity.getAppContext(), resp.requests);
            }
        }
    };

    public synchronized void handleFirebaseMsg(String jsonStr) {
        mBus.register(this);
        try {
            JSONObject json = new JSONObject(jsonStr);

            String cmd = json.getString("command");
            switch (cmd) {
                case FriendReqMsg._command:
                    FriendController.getInstance().syncFriends(
                            friendReqCallback,
                            MainActivity.getAppContext(),
                            DataController.getInstance().getUser(MainActivity.getAppContext()).uid);
                    break;
                case FriendAckMsg._command:
                    FriendController.getInstance().syncFriends(
                            friendReqCallback,
                            MainActivity.getAppContext(),
                            DataController.getInstance().getUser(MainActivity.getAppContext()).uid);
                    break;
                case ChatMsg._command:
                    //TODO: put to msg history and display
                    ChatMsg chatMsg = new Gson().fromJson(jsonStr, ChatMsg.class);
                    handleChatMsg(chatMsg);
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private synchronized void handleChatMsg(final ChatMsg msg) throws IOException {
        final Context context = MainActivity.getAppContext();
        switch (msg.msgType) {
            case TEXT:
                Envelop envelop = new Envelop(
                        DataController.getInstance().getContactById(context, msg.sender),
                        DataController.getInstance().getUser(context),
                        Envelop.EnvelopStatus.DELIVERED,
                        msg.msgLife,
                        msg);
                DataController.getInstance().appendEnvelopToChatSession(
                        context,
                        DataController.getInstance().getContactById(context, msg.sender),
                        envelop);
                mBus.post(envelop);
                break;
            case IMAGE:
            case VIDEO:
                StorageReference ref = FirebaseStorage.getInstance().getReferenceFromUrl(msg.msgStr);
                String onlyUrl = msg.msgStr.substring(0, msg.msgStr.lastIndexOf("?"));
                String fileName = FilenameUtils.getName(onlyUrl);

                File localFile = new File(context.getCacheDir(), fileName);
                final String filePath = localFile.getAbsolutePath();
                MyFirebaseStorageManager.getInstance().downloadFile(
                        new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                                Envelop envelop = new Envelop(
                                        DataController.getInstance().getContactById(context, msg.sender),
                                        DataController.getInstance().getUser(context),
                                        Envelop.EnvelopStatus.DELIVERED,
                                        msg.msgLife,
                                        msg,
                                        filePath);
                                DataController.getInstance().appendEnvelopToChatSession(
                                        context,
                                        DataController.getInstance().getContactById(context, msg.sender),
                                        envelop);
                                mBus.post(envelop);
                            }
                        },
                        ref,
                        localFile
                );
                break;
            case WITHDRAW:
                Envelop envelop1 = new Envelop(
                        DataController.getInstance().getContactById(context, msg.sender),
                        DataController.getInstance().getUser(context),
                        Envelop.EnvelopStatus.DELIVERED,
                        msg.msgLife,
                        msg);
                DataController.getInstance().deleteMostRecentEnvelopFromChatSession(context,msg.sender);
                mBus.post(envelop1);
                break;
        }
    }
}
