package com.uom.cryz.snapmessenger.models;

import android.media.Image;
import android.provider.MediaStore;

import com.uom.cryz.snapmessenger.models.messages.ChatMsg;

/**
 * Created by ruijiankun on 2/10/2016.
 */
@Deprecated
public class Message {
    public int id;
    public ChatMsg.MsgType type;
    public String msg;
    public Image image;
    public MediaStore.Video video;


    public Message(int id, String msg) {
        this.type = ChatMsg.MsgType.TEXT;
        this.id = id;
        this.msg = msg;
    }

    public Message(int id, Image img) {
        this.type = ChatMsg.MsgType.IMAGE;
        this.id = id;
        this.image = img;
    }

    public Message(int id, MediaStore.Video video) {
        this.type = ChatMsg.MsgType.VIDEO;
        this.id = id;
        this.video = video;
    }
}
