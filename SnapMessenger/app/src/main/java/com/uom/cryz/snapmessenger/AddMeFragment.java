package com.uom.cryz.snapmessenger;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amigold.fundapter.BindDictionary;
import com.amigold.fundapter.FunDapter;
import com.amigold.fundapter.extractors.StringExtractor;
import com.uom.cryz.snapmessenger.controllers.DataController;
import com.uom.cryz.snapmessenger.controllers.FriendController;
import com.uom.cryz.snapmessenger.controllers.MyVolley;
import com.uom.cryz.snapmessenger.models.Contact;
import com.uom.cryz.snapmessenger.models.messages.FriendAckMsg;
import com.uom.cryz.snapmessenger.models.responses.FriendGetResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddMeFragment extends Fragment {

    private static final String TAG = AddMeFragment.class.getSimpleName();
    String uid;
    String nickname;
    View myView;

    private void refresh() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(this);
        ft.attach(this);
        ft.commit();
    }

    public AddMeFragment() {
        // Required empty public constructor
    }
    private class MyListViewAdapter extends ArrayAdapter<Contact>{
        private int layout;
        public MyListViewAdapter(Context context, int resource, List<Contact> objects) {
            super(context, resource, objects);
            layout = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final Contact receiveContact = getItem(position);
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(layout,parent,false);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.nickname = (TextView) convertView.findViewById(R.id.addMeNickNameTV);
            viewHolder.uid = (TextView) convertView.findViewById(R.id.addMeUidTV);
            viewHolder.addMeAddBtn = (Button) convertView.findViewById(R.id.addMeAddBtn);
            viewHolder.addMeAddBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    FriendController.getInstance().accept(
                            new MyVolley.VolleyCallback() {
                                    @Override
                                    public void onSuccess(Object result) {
                                    Toast.makeText(getActivity().getApplicationContext(), "Friend added!", Toast.LENGTH_SHORT).show();
                                        FriendController.getInstance().syncFriends(
                                                new MyVolley.VolleyCallback() {
                                                    @Override
                                                    public void onSuccess(Object result) {
                                                        FriendGetResponse resp = (FriendGetResponse) result;
                                                        if (resp.success) {
                                                            DataController.getInstance().saveFriendReq(getActivity().getApplicationContext(), resp.requests);
                                                            refresh();
                                                        }
                                                    }
                                                },
                                                MainActivity.getAppContext(),
                                                DataController.getInstance().getUser(MainActivity.getAppContext()).uid);

                                }
                                },
                            getActivity().getApplicationContext(),
                            uid,
                            receiveContact.uid,
                            new FriendAckMsg(true, uid,  receiveContact.uid, nickname)
                    );
                }
                });
            //convertView.setTag(viewHolder);
            viewHolder.nickname.setText(getItem(position).nick);
            viewHolder.uid.setText(getItem(position).uid);
            return convertView;
        }
    }
    public class ViewHolder{
        TextView nickname;
        TextView uid;
        Button addMeAddBtn;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        uid = DataController.getInstance().getUser(getActivity().getApplicationContext()).uid;
        nickname = DataController.getInstance().getUser(getActivity().getApplicationContext()).nick;
        ArrayList<Contact> contacts = DataController.getInstance().getFriendReq(getActivity().getApplicationContext());
        myView = inflater.inflate(R.layout.fragment_add_me, container, false);
        assignViewActions();
        ListView addMeLV = (ListView) myView.findViewById(R.id.addMeLV);
        addMeLV.setAdapter(new MyListViewAdapter(getActivity(),R.layout.addme_list_pattern,contacts) );


        /*BindDictionary<Contact> dictionary = new BindDictionary<>();
        dictionary.addStringField(R.id.addMeNickNameTV, new StringExtractor<Contact>() {
            @Override
            public String getStringValue(Contact item, int position) {
                return item.nick;
            }
        });
        dictionary.addStringField(R.id.addMeUidTV, new StringExtractor<Contact>() {
            @Override
            public String getStringValue(Contact item, int position) {
                return item.uid;
            }
        });

        FunDapter adapter = new FunDapter(getActivity(), contacts, R.layout.addme_list_pattern, dictionary);
        final ListView addMeLV = (ListView) myView.findViewById(R.id.addMeLV);

        addMeLV.setAdapter(adapter);
        adapter.notifyDataSetChanged();


        final String uid = DataController.getInstance().getUser(getActivity().getApplicationContext()).uid;
        final String nickname = DataController.getInstance().getUser(getActivity().getApplicationContext()).nick;
        addMeLV.
        Button addMeBtn = (Button) myView.findViewById(R.id.addMeAddBtn);
        addMeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    View parentRow = (View)v.getParent();
                    ListView listView = (ListView) parentRow.getParent();
                    int position = listView.getPositionForView(parentRow);
                    Contact receiveContact = contacts.get(position);
                    FriendController.getInstance().accept(
                            new MyVolley.VolleyCallback() {
                                @Override
                                public void onSuccess(Object result) {
                                    Toast.makeText(getActivity().getApplicationContext(), "Friend added!", Toast.LENGTH_SHORT).show();
                                    refresh();
                                }
                            },
                            getActivity().getApplicationContext(),
                            uid,
                            receiveContact.uid,
                            new FriendAckMsg(true, uid, receiveContact.uid, nickname)
                    );
                }
            });

*/

        return myView;
    }

    private void assignViewActions() {
        ImageView imgBack = (ImageView) myView.findViewById(R.id.imgview_add_me_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment emptyFragment = new EmptyFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(
                        R.id.layout_content_main,
                        emptyFragment
                ).commit();
                ViewPager pager = (ViewPager) getActivity().findViewById(R.id.pager);
                pager.setVisibility(View.VISIBLE);
            }
        });
    }
}
