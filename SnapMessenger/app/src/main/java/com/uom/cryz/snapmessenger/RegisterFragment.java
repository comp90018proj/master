package com.uom.cryz.snapmessenger;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.uom.cryz.snapmessenger.controllers.AccountController;
import com.uom.cryz.snapmessenger.controllers.DataController;
import com.uom.cryz.snapmessenger.controllers.MyVolley;
import com.uom.cryz.snapmessenger.models.AppUser;
import com.uom.cryz.snapmessenger.models.responses.LoginResponse;
import com.uom.cryz.snapmessenger.models.responses.RegisterResponse;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {
    private static final String TAG = RegisterFragment.class.getSimpleName();

    View myView;

    MyVolley.VolleyCallback callback = new MyVolley.VolleyCallback() {
        @Override
        public void onSuccess(Object result) {
            RegisterResponse resp = (RegisterResponse) result;
            Log.d(TAG, resp.success.toString());
            if (resp.success){
                AppUser user = new AppUser(resp.uid, resp.email, resp.nick, resp.mobile,true);
                DataController.getInstance().saveUser( getActivity().getApplicationContext(),user);
                Intent i = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                startActivity(i);
                Log.d(TAG, "success Register");
            }else{
                Toast.makeText(getActivity().getApplicationContext(), "Register Failed. Please Retry.", Toast.LENGTH_SHORT).show();
            }

        }
    };
    public RegisterFragment() {}



        @Override
        public View onCreateView (LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState){
            myView = inflater.inflate(R.layout.fragment_register, container, false);
            Button button = (Button) myView.findViewById(R.id.registerBtn);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String email = ((EditText) myView.findViewById(R.id.registerEmailET)).getText().toString();
                    String password1 = ((EditText) myView.findViewById(R.id.register1stPwdET)).getText().toString();
                    String password2 = ((EditText) myView.findViewById(R.id.register2ndPwdET)).getText().toString();
                    String nickName = ((EditText) myView.findViewById(R.id.registerNickNameET)).getText().toString();
                    String mobile = ((EditText) myView.findViewById(R.id.mobileET)).getText().toString();
                    if( password1.equals(password2)){
                        try {
                            AccountController.getInstance().register(callback, getActivity().getApplicationContext(),email, nickName,password1,mobile);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        }
                    } else{
                        Toast.makeText(getActivity().getApplicationContext(), "Different Password Entered", Toast.LENGTH_SHORT).show();

                    }

                }
            });

            return myView;
        }

    }

