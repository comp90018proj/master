package com.uom.cryz.snapmessenger;


import android.content.Context;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.uom.cryz.snapmessenger.controllers.DataController;
import com.uom.cryz.snapmessenger.controllers.FriendController;
import com.uom.cryz.snapmessenger.controllers.GeoInfoController;
import com.uom.cryz.snapmessenger.controllers.MyVolley;
import com.uom.cryz.snapmessenger.models.Contact;
import com.uom.cryz.snapmessenger.models.NearbyFriend;
import com.uom.cryz.snapmessenger.models.messages.FriendAckMsg;
import com.uom.cryz.snapmessenger.models.messages.FriendReqMsg;
import com.uom.cryz.snapmessenger.models.responses.FriendGetResponse;
import com.uom.cryz.snapmessenger.models.responses.NearbyFriendResponse;
import com.uom.cryz.snapmessenger.models.responses.ServerResponse;
import com.uom.cryz.snapmessenger.util.GeoDistance;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class TinderFragment extends Fragment {
    private View myView;
    private ArrayList<NearbyFriend> nearbyFriends= new ArrayList<>();
    private MyListViewAdapter tinderLVAdapter;

    private static final String TAG = TinderFragment.class.getSimpleName();
    public TinderFragment() {
        // Required empty public constructor
    }


    private class MyListViewAdapter extends ArrayAdapter<NearbyFriend> {
        private int layout;
        public MyListViewAdapter(Context context, int resource, List<NearbyFriend> objects) {
            super(context, resource, objects);
            layout = resource;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(layout, parent, false);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.nickname = (TextView) convertView.findViewById(R.id.tinderNickNameTV);
            viewHolder.uid = (TextView) convertView.findViewById(R.id.tinderUidTV);
            viewHolder.distance = (TextView) convertView.findViewById(R.id.tinderDistanceTV);
            viewHolder.tinderAddBtn = (Button) convertView.findViewById(R.id.tinderAddBtn);
            final Contact sender = DataController.getInstance().getUser(getActivity().getApplicationContext());
            viewHolder.tinderAddBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FriendController.getInstance().addById(new MyVolley.VolleyCallback() {
                        @Override
                        public void onSuccess(Object result) {
                            ServerResponse resp = (ServerResponse) result;
                            if (resp.success) {
                                Toast.makeText(getActivity().getApplicationContext(), "Friend request sent!", Toast.LENGTH_SHORT).show();
                                viewHolder.tinderAddBtn.setEnabled(false);
                            } else {
                                Toast.makeText(getActivity().getApplicationContext(), "Failed sending friend request!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, getActivity().getApplicationContext(), sender.uid, getItem(position).uid, new FriendReqMsg(sender.uid, getItem(position).uid, sender.nick));
                }
            });
            viewHolder.nickname.setText(getItem(position).nick);
            viewHolder.uid.setText(getItem(position).uid);
            String distanceStr = "Calculating";
            if (GeoInfoController.getInstance(getActivity().getApplicationContext()).lastLocation != null) {
                Double myLatitude = GeoInfoController.getInstance(getActivity().getApplicationContext()).lastLocation.getLatitude();
                Double myLongitude = GeoInfoController.getInstance(getActivity().getApplicationContext()).lastLocation.getLongitude();
                Double receiverLatitude = getItem(position).lat;
                Double receiverLongitude = getItem(position).lng;
                Double distance = GeoDistance.calculateDistance(myLatitude, myLongitude, receiverLatitude, receiverLongitude);
                distanceStr = String.format("%.2f m", distance);
            }
            viewHolder.distance.setText(distanceStr);
            return convertView;
        }
    }
    public class ViewHolder{
        TextView nickname;
        TextView uid;
        TextView distance;
        Button tinderAddBtn;

    }


    MyVolley.VolleyCallback getGeoCallback = new MyVolley.VolleyCallback() {
        @Override
        public void onSuccess(Object result) {
            NearbyFriendResponse resp = (NearbyFriendResponse) result;
            if(resp.success)
            {
                nearbyFriends = resp.contacts;
                for (NearbyFriend nearByFriend : nearbyFriends){
                    tinderLVAdapter.add(nearByFriend);
                }
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView =inflater.inflate(R.layout.fragment_tinder, container, false);
        assignViewActions();
        GeoInfoController.getInstance(getActivity().getApplicationContext()).getNearbyFriends(getGeoCallback,getActivity().getApplicationContext());
        ListView tinderLV = (ListView)myView.findViewById(R.id.tinderLV);
        tinderLVAdapter =new MyListViewAdapter(getActivity(), R.layout.tinder_list_pattern,nearbyFriends);
        tinderLVAdapter.notifyDataSetChanged();
        tinderLV.setAdapter(tinderLVAdapter);
        return myView;
    }

    private void assignViewActions() {
        ImageView imgBack = (ImageView) myView.findViewById(R.id.imgview_tinder_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment emptyFragment = new EmptyFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(
                        R.id.layout_content_main,
                        emptyFragment
                ).commit();
                ViewPager pager = (ViewPager) getActivity().findViewById(R.id.pager);
                pager.setVisibility(View.VISIBLE);
            }
        });
    }
}
