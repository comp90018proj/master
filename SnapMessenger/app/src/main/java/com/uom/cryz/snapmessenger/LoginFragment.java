package com.uom.cryz.snapmessenger;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.uom.cryz.snapmessenger.controllers.AccountController;
import com.uom.cryz.snapmessenger.controllers.DataController;
import com.uom.cryz.snapmessenger.controllers.FriendController;
import com.uom.cryz.snapmessenger.controllers.GeoInfoController;
import com.uom.cryz.snapmessenger.controllers.MessageController;
import com.uom.cryz.snapmessenger.controllers.MyVolley;
import com.uom.cryz.snapmessenger.fbservices.MyFirebaseInstanceIDService;
import com.uom.cryz.snapmessenger.models.AppUser;
import com.uom.cryz.snapmessenger.models.Contact;
import com.uom.cryz.snapmessenger.models.responses.FriendGetResponse;
import com.uom.cryz.snapmessenger.models.responses.LoginResponse;
import com.uom.cryz.snapmessenger.models.responses.ServerResponse;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {
    View myView;
    private static final String TAG = LoginFragment.class.getSimpleName();

    public LoginFragment() {
        // Required empty public constructor
    }

    MyVolley.VolleyCallback syncCallback = new MyVolley.VolleyCallback() {
        @Override
        public void onSuccess(Object result) {
            FriendGetResponse resp = (FriendGetResponse) result;
            if (resp.success) {
                DataController.getInstance().saveContacts(getActivity().getApplicationContext(), resp.contacts);
                DataController.getInstance().saveFriendReq(getActivity().getApplicationContext(), resp.requests);
                Intent i = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                startActivity(i);
                Log.d(TAG, "success Login");
            } else {
                Toast.makeText(getActivity().getApplicationContext(), "Login Failed. Please Check Input.", Toast.LENGTH_SHORT).show();

            }
        }
    };


    MyVolley.VolleyCallback callback = new MyVolley.VolleyCallback() {
        @Override
        public void onSuccess(Object result) {
            LoginResponse resp = (LoginResponse) result;
            if (resp.success) {
                AppUser user = new AppUser(resp.uid, resp.email, resp.nick, resp.mobile, true);
                DataController.getInstance().saveUser(getActivity().getApplicationContext(), user);
                DataController.getInstance().login(getActivity().getApplicationContext());
                FriendController.getInstance().syncFriends(syncCallback, getActivity().getApplicationContext(), user.uid);

            } else {
                Toast.makeText(getActivity().getApplicationContext(), "Login Failed. Please Check Input.", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_login, container, false);
        Button button = (Button) myView.findViewById(R.id.loginBtn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = ((EditText) myView.findViewById(R.id.loginEmailET)).getText().toString();
                String password = ((EditText) myView.findViewById(R.id.loginPwdET)).getText().toString();
                try {
                    AccountController.getInstance().login(callback, getActivity().getApplicationContext(), email, password);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }

            }
        });


        return myView;
    }

}
