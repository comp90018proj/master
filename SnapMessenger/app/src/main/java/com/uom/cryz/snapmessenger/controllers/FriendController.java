package com.uom.cryz.snapmessenger.controllers;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.uom.cryz.snapmessenger.models.messages.FriendAckMsg;
import com.uom.cryz.snapmessenger.models.messages.FriendReqMsg;
import com.uom.cryz.snapmessenger.models.messages.JsonMsg;
import com.uom.cryz.snapmessenger.models.responses.FriendGetResponse;
import com.uom.cryz.snapmessenger.models.responses.FriendSearchResponse;
import com.uom.cryz.snapmessenger.models.responses.ServerResponse;
import com.uom.cryz.snapmessenger.util.NetworkChecker;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ruijiankun on 7/10/2016.
 */
public class FriendController {
    private static final String FRIEND_ADD_URL = "http://jackrui.uicp.hk/comp90018/friend_add.php";
    private static final String FRIEND_GET_URL = "http://jackrui.uicp.hk/comp90018/friend_get.php";
    private static final String FRIEND_BLOCK_URL = "http://jackrui.uicp.hk/comp90018/friend_block.php";
    private static final String FRIEND_REMARK_URL = "http://jackrui.uicp.hk/comp90018/friend_remark.php";
    private static final String FRIEND_REMOVE_URL = "http://jackrui.uicp.hk/comp90018/friend_remove.php";
    private static final String FRIEND_SEARCH_URL = "http://jackrui.uicp.hk/comp90018/friend_query.php";

    private static final String TAG = FriendController.class.getSimpleName();

    private static FriendController controller;
    public static FriendController getInstance() {
        if (controller == null) {
            controller = new FriendController();
        }
        return controller;
    }
    public FriendController() {
    }

    public enum RequestType {
        BLOCK,
        UNBLOCK,
        REQUEST,
        ACCEPT,
        DENY
    }

    public void addById(final MyVolley.VolleyCallback callback, final Context context, final String senderUid, final String receiverUid, final FriendReqMsg msg) {
        handleReq(callback, context, senderUid, receiverUid, RequestType.REQUEST, msg);
    }

    public void accept(final MyVolley.VolleyCallback callback, final Context context, final String senderUid, final String receiverUid, final FriendAckMsg msg) {
        handleReq(callback, context, senderUid, receiverUid, RequestType.ACCEPT, msg);
    }

    public void deny(final MyVolley.VolleyCallback callback, final Context context, final String senderUid, final String receiverUid, final FriendAckMsg msg) {
        handleReq(callback, context, senderUid, receiverUid, RequestType.DENY, msg);
    }


    private void handleReq(final MyVolley.VolleyCallback callback, final Context context, final String senderUid, final String receiverUid, final RequestType type, final JsonMsg msg) {
        if(!NetworkChecker.isNetworkOK(context))
        {
            Toast.makeText(context,"Network Error!", Toast.LENGTH_SHORT).show();
            callback.onSuccess(new ServerResponse(false));
            return;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, FRIEND_ADD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);

                        ServerResponse resp = new Gson().fromJson(response, ServerResponse.class);
                        callback.onSuccess(resp);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.getStackTrace().toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("from", senderUid);
                headers.put("to", receiverUid);
                switch (type) {
                    case REQUEST:
                        headers.put("status", "1");
                        break;
                    case ACCEPT:
                        headers.put("status", "2");
                        break;
                    case DENY:
                        headers.put("status", "3");
                        break;
                }
                return headers;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return msg.toJsonString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", msg.toJsonString(), "utf-8");
                    return null;
                }
            }
        };
        MyVolley.getInstance(context).addToRequestQueue(stringRequest);
    }



    public void block(final MyVolley.VolleyCallback callback, final Context context, final String senderUid, final String receiverUid){
        handleBlock(callback,context,senderUid,receiverUid,RequestType.BLOCK);
    }

    public void unblock(final MyVolley.VolleyCallback callback, final Context context, final String senderUid, final String receiverUid){
        handleBlock(callback,context,senderUid,receiverUid,RequestType.UNBLOCK);
    }

    private void handleBlock(final MyVolley.VolleyCallback callback, final Context context, final String senderUid, final String receiverUid, final RequestType type) {
        if(!NetworkChecker.isNetworkOK(context))
        {
            Toast.makeText(context,"Network Error!", Toast.LENGTH_SHORT).show();
            callback.onSuccess(new ServerResponse(false));
            return;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, FRIEND_BLOCK_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        ServerResponse resp = new Gson().fromJson(response, ServerResponse.class);
                        callback.onSuccess(resp);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.getStackTrace().toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("from", senderUid);
                headers.put("to", receiverUid);
                if(type == RequestType.BLOCK) {
                    headers.put("blockStatus", "-1");
                }
                else if(type == RequestType.UNBLOCK){
                    headers.put("blockStatus", "0");
                }
                return headers;
            }
        };
        MyVolley.getInstance(context).addToRequestQueue(stringRequest);
    }

    public void remark(final MyVolley.VolleyCallback callback, final Context context, final String senderUid, final String friendUid, final String remark){
        if(!NetworkChecker.isNetworkOK(context))
        {
            Toast.makeText(context,"Network Error!", Toast.LENGTH_SHORT).show();
            callback.onSuccess(new ServerResponse(false));
            return;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, FRIEND_REMARK_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        ServerResponse resp = new Gson().fromJson(response, ServerResponse.class);
                        callback.onSuccess(resp);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.getStackTrace().toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("uid", senderUid);
                headers.put("friendId", friendUid);
                headers.put("remark", remark);
                return headers;
            }
        };
        MyVolley.getInstance(context).addToRequestQueue(stringRequest);
    }

    public void remove(final MyVolley.VolleyCallback callback, final Context context, final String senderUid, final String friendUid){
        if(!NetworkChecker.isNetworkOK(context))
        {
            Toast.makeText(context,"Network Error!", Toast.LENGTH_SHORT).show();
            callback.onSuccess(new ServerResponse(false));
            return;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, FRIEND_REMOVE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        ServerResponse resp = new Gson().fromJson(response, ServerResponse.class);
                        callback.onSuccess(resp);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.getStackTrace().toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("uid", senderUid);
                headers.put("friendId", friendUid);
                return headers;
            }
        };
        MyVolley.getInstance(context).addToRequestQueue(stringRequest);
    }

    public void syncFriends(final MyVolley.VolleyCallback callback, final Context context, final String uid){
        if(!NetworkChecker.isNetworkOK(context))
        {
            Toast.makeText(context,"Network Error!", Toast.LENGTH_SHORT).show();
            callback.onSuccess(new FriendGetResponse(false));
            return;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, FRIEND_GET_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        FriendGetResponse resp = new Gson().fromJson(response, FriendGetResponse.class);
                        callback.onSuccess(resp);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.getStackTrace().toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("uid", uid);
                return headers;
            }
        };
        MyVolley.getInstance(context).addToRequestQueue(stringRequest);
    }

    public void searchFriend(final MyVolley.VolleyCallback callback, final Context context, final String targetUid){
        if(!NetworkChecker.isNetworkOK(context))
        {
            Toast.makeText(context,"Network Error!", Toast.LENGTH_SHORT).show();
            callback.onSuccess(new FriendSearchResponse(false));
            return;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, FRIEND_SEARCH_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        FriendSearchResponse resp = new Gson().fromJson(response, FriendSearchResponse.class);
                        callback.onSuccess(resp);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.getStackTrace().toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("uid", targetUid);
                return headers;
            }
        };
        MyVolley.getInstance(context).addToRequestQueue(stringRequest);
    }
}
