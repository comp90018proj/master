package com.uom.cryz.snapmessenger.models.responses;

import com.uom.cryz.snapmessenger.models.Contact;

/**
 * Created by ruijiankun on 9/10/2016.
 */
public class FriendSearchResponse extends ServerResponse {
    public Contact contact;
    public FriendSearchResponse(Boolean success)
    {
        super(success);
    }
}
