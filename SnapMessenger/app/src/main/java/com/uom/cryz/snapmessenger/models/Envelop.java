package com.uom.cryz.snapmessenger.models;

import android.app.Application;
import android.content.Context;

import com.uom.cryz.snapmessenger.controllers.DataController;
import com.uom.cryz.snapmessenger.models.messages.ChatMsg;

import java.security.Timestamp;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ruijiankun on 4/10/2016.
 */
public class Envelop {
    public enum EnvelopStatus {
        SENT,
        DELIVERED,
        OPENED,
    }

    public Contact from;
    public Contact to;
    public Date createTime;
    public EnvelopStatus status = EnvelopStatus.SENT;
    public Date openTime;
    public long expireSeconds=20;
    public String resourceLink="";
    public ChatMsg msg;

    public Envelop(Contact from, Contact to, EnvelopStatus status, long expTime, ChatMsg msg, String resourceLink) {
        this.from = from;
        this.to = to;
        this.createTime = Calendar.getInstance().getTime();
        this.status = status;
        this.expireSeconds = expTime;
        this.msg = msg;
        this.resourceLink = resourceLink;
    }

    public Envelop(Contact from, Contact to, EnvelopStatus status, long expTime, ChatMsg msg) {
        this.from = from;
        this.to = to;
        this.createTime = Calendar.getInstance().getTime();
        this.status = status;
        this.expireSeconds = expTime;
        this.msg = msg;
    }

    public void open() {

        this.status = EnvelopStatus.OPENED;
        openTime = Calendar.getInstance().getTime();
    }

    //TOOD: comparable?

}
