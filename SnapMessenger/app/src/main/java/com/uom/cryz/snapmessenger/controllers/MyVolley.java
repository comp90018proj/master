package com.uom.cryz.snapmessenger.controllers;

import android.content.Context;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.uom.cryz.snapmessenger.models.Envelop;
import com.uom.cryz.snapmessenger.models.responses.ServerResponse;

/**
 * Created by ruijiankun on 7/10/2016.
 */
public class MyVolley extends Volley {
    private static MyVolley mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;

    private MyVolley(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized MyVolley getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MyVolley(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public interface VolleyCallback {
        void onSuccess(Object result);
    }

    public interface VolleySendCallback {
        void onSentSuccess(ServerResponse result, Envelop envelop);
    }
}


