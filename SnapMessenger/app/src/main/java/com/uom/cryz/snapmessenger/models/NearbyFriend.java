package com.uom.cryz.snapmessenger.models;

import java.util.Date;

/**
 * Created by ruijiankun on 18/10/2016.
 */
public class NearbyFriend extends Contact {
    public double lng = 0.0;
    public double lat = 0.0;
    public String lastUpdate;

    public NearbyFriend(String id, String email, String nick, String remark, String mobile, String token) {
        super(id, email, nick, remark, mobile, token);
    }
}
