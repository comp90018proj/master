package com.uom.cryz.snapmessenger;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amigold.fundapter.BindDictionary;
import com.amigold.fundapter.FunDapter;
import com.amigold.fundapter.extractors.StringExtractor;
import com.uom.cryz.snapmessenger.controllers.AccountController;
import com.uom.cryz.snapmessenger.controllers.DataController;
import com.uom.cryz.snapmessenger.controllers.FriendController;
import com.uom.cryz.snapmessenger.controllers.MyVolley;
import com.uom.cryz.snapmessenger.models.Contact;
import com.uom.cryz.snapmessenger.models.messages.FriendAckMsg;
import com.uom.cryz.snapmessenger.models.responses.FriendGetResponse;
import com.uom.cryz.snapmessenger.models.responses.ServerResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyFriendsFragment extends Fragment {
    private static final String TAG = MyFriendsFragment.class.getSimpleName();
    View myView;
    String uid;
    String nickname;
    String lastPage = "profile";

    private void refresh() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(this);
        ft.attach(this);
        ft.commit();
    }

    public MyFriendsFragment() {
        // Required empty public constructor
    }

    private class MyFriendsListViewAdapter extends ArrayAdapter<Contact> {
        private int layout;

        public MyFriendsListViewAdapter(Context context, int resource, List<Contact> objects) {
            super(context, resource, objects);
            layout = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //final Contact receiveContact = getItem(position);
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(layout, parent, false);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.nickname = (TextView) convertView.findViewById(R.id.myFriendsListNickNameTV);
            viewHolder.uid = (TextView) convertView.findViewById(R.id.myFriendsListUidTV);
            viewHolder.nickname.setText(getItem(position).nick);
            viewHolder.uid.setText(getItem(position).uid);
            return convertView;
        }
    }

    public class ViewHolder {
        TextView nickname;
        TextView uid;
        //Button addMeAddBtn;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        lastPage = getArguments().getString("fromPage");

        uid = DataController.getInstance().getUser(getActivity().getApplicationContext()).uid;
        nickname = DataController.getInstance().getUser(getActivity().getApplicationContext()).nick;
        myView = inflater.inflate(R.layout.fragment_my_friends, container, false);
        assignViewActions();

        final ArrayList<Contact> contacts = DataController.getInstance().getContacts(getActivity().getApplicationContext());
        ListView myFriendsLV = (ListView) myView.findViewById(R.id.myFriendsLV);
        myFriendsLV.setAdapter(new MyFriendsListViewAdapter(getActivity(), R.layout.myfriends_list_pattern, contacts));

        myFriendsLV.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                        final Dialog dialog = new Dialog(getActivity());
                        dialog.setContentView(R.layout.myfriends_dialog_layout);
                        TextView nicknameTV = (TextView) dialog.findViewById(R.id.myFriendsNickNameTV);
                        TextView uidTV = (TextView) dialog.findViewById(R.id.myFriendsUidTV);
                        Button removeBtn = (Button) dialog.findViewById(R.id.myFriendsRemoveBtn);
                        Button blockBtn = (Button) dialog.findViewById(R.id.myFriendsBlockBtn);
                        Button unblockBtn = (Button) dialog.findViewById(R.id.myFriendsUnblockBtn);
                        final Button chatBtn = (Button) dialog.findViewById(R.id.myFriendsChatBtn);
                        final Contact receiver = contacts.get(position);
                        int isBlocked = receiver.blocked;
                        if (isBlocked == 0) {
                            unblockBtn.setEnabled(false);
                            unblockBtn.setVisibility(View.GONE);
                            blockBtn.setEnabled(true);
                            blockBtn.setVisibility(View.VISIBLE);
                        } else {
                            unblockBtn.setEnabled(true);
                            unblockBtn.setVisibility(View.VISIBLE);
                            blockBtn.setEnabled(false);
                            blockBtn.setVisibility(View.GONE);
                        }
                        nicknameTV.setText(receiver.nick);
                        uidTV.setText(receiver.uid);
                        removeBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                FriendController.getInstance().remove(new MyVolley.VolleyCallback() {
                                    @Override
                                    public void onSuccess(Object result) {
                                        Toast.makeText(getActivity().getApplicationContext(), "Friend is removed!", Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();
                                        FriendController.getInstance().syncFriends(
                                                new MyVolley.VolleyCallback() {
                                                    @Override
                                                    public void onSuccess(Object result) {
                                                        FriendGetResponse resp = (FriendGetResponse) result;
                                                        if (resp.success) {
                                                            DataController.getInstance().saveContacts(getActivity().getApplicationContext(), resp.requests);
                                                            refresh();
                                                        }
                                                    }
                                                },
                                                MainActivity.getAppContext(),
                                                DataController.getInstance().getUser(MainActivity.getAppContext()).uid);
                                    }
                                }, getActivity().getApplicationContext(), uid, receiver.uid);
                            }
                        });
                        blockBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                FriendController.getInstance().block(new MyVolley.VolleyCallback() {
                                    @Override
                                    public void onSuccess(Object result) {
                                        Toast.makeText(getActivity().getApplicationContext(), "Friend is blocked!", Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();
                                        FriendController.getInstance().syncFriends(
                                                new MyVolley.VolleyCallback() {
                                                    @Override
                                                    public void onSuccess(Object result) {
                                                        FriendGetResponse resp = (FriendGetResponse) result;
                                                        if (resp.success) {
                                                            DataController.getInstance().saveContacts(getActivity().getApplicationContext(), resp.requests);
                                                            refresh();
                                                        }
                                                    }
                                                },
                                                MainActivity.getAppContext(),
                                                DataController.getInstance().getUser(MainActivity.getAppContext()).uid);
                                    }
                                }, getActivity().getApplicationContext(), uid, receiver.uid);
                            }
                        });
                        unblockBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                FriendController.getInstance().unblock(new MyVolley.VolleyCallback() {
                                    @Override
                                    public void onSuccess(Object result) {
                                        Toast.makeText(getActivity().getApplicationContext(), "Friend is unblocked!", Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();
                                        FriendController.getInstance().syncFriends(
                                                new MyVolley.VolleyCallback() {
                                                    @Override
                                                    public void onSuccess(Object result) {
                                                        FriendGetResponse resp = (FriendGetResponse) result;
                                                        if (resp.success) {
                                                            DataController.getInstance().saveContacts(getActivity().getApplicationContext(), resp.requests);
                                                            refresh();
                                                        }
                                                    }
                                                },
                                                MainActivity.getAppContext(),
                                                DataController.getInstance().getUser(MainActivity.getAppContext()).uid);
                                        refresh();
                                    }
                                }, getActivity().getApplicationContext(), uid, receiver.uid);
                            }
                        });
                        chatBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                Bundle bundle = new Bundle();
                                bundle.putString("uid", receiver.uid);
                                FragmentTransaction ft = getActivity().getSupportFragmentManager()
                                        .beginTransaction();
                                ChatFragment chatFragment = new ChatFragment();
                                chatFragment.setArguments(bundle);
                                ft.replace(R.id.layout_content_main, chatFragment, chatFragment.getTag());
                                ft.commit();
                            }
                        });
                        dialog.show();
                    }
                }
        );

        return myView;
    }

    private void assignViewActions() {
        ImageView imgBack = (ImageView) myView.findViewById(R.id.imgview_my_friends_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lastPage.equals(ProfileFragment.TAG)) {
                    Fragment emptyFragment = new EmptyFragment();
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    fm.beginTransaction().replace(
                            R.id.layout_content_main,
                            emptyFragment
                    ).commit();
                    ViewPager pager = (ViewPager) getActivity().findViewById(R.id.pager);
                    pager.setVisibility(View.VISIBLE);
                } else if (lastPage.equals(SessionListFragment.TAG)) {
                    Fragment emptyFragment = new EmptyFragment();
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    fm.beginTransaction().replace(
                            R.id.layout_content_main,
                            emptyFragment
                    ).commit();
                    ViewPager pager = (ViewPager) getActivity().findViewById(R.id.pager);
                    ViewPager vpager = (ViewPager) getActivity().findViewById(R.id.vpager);
                    vpager.setCurrentItem(1);
                    pager.setCurrentItem(0);
                    pager.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}