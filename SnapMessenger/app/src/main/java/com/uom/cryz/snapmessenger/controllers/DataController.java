package com.uom.cryz.snapmessenger.controllers;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.uom.cryz.snapmessenger.models.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by ruijiankun on 4/10/2016.
 *
 * Control all locate data store
 */
public class DataController {

    private static final String TAG = DataController.class.getSimpleName();
    private static DataController dataController = null;

    public static synchronized DataController getInstance() {
        if (dataController == null) {
            dataController = new DataController();
        }
        return dataController;
    }

    public DataController() {
    }

    public synchronized void resetUserData(Context context) {
        saveContacts(context, new ArrayList<Contact>());
        saveFriendReq(context, new ArrayList<Contact>());
        delAllSessions(context);
        saveUser(context, new AppUser("", "", "", "", false));
    }

    public synchronized ArrayList<Contact> getContacts(Context context) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences("Contacts", 0);
        String result = sp.getString("contacts", "");
        ArrayList<Contact> contacts = new ArrayList<>();

        if (!result.isEmpty()) {
            Type listType = new TypeToken<ArrayList<Contact>>() {
            }.getType();
            ArrayList<Contact> list = new Gson().fromJson(result, listType);
            contacts = list;
        }
        return contacts;
    }

    public synchronized Contact getContactById(Context context, String uid) {

        ArrayList<Contact> contacts = getContacts(context);
        for (Contact c : contacts) {
            if (c.uid.equals(uid)) {
                return c;
            }
        }
        return null;
    }

    public synchronized ArrayList<Contact> saveContacts(Context context, ArrayList<Contact> contacts) {
        Type listType = new TypeToken<ArrayList<Contact>>() {
        }.getType();
        String reqStr = new Gson().toJson(contacts, listType);

        SharedPreferences sp = context.getApplicationContext().getSharedPreferences("Contacts", 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("contacts", reqStr);
        editor.apply();

        return null;
    }

    public synchronized ArrayList<Contact> getFriendReq(Context context) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences("FriendReq", 0);
        String result = sp.getString("requests", "");
        ArrayList<Contact> requests = new ArrayList<>();

        if (!result.isEmpty()) {
            Type listType = new TypeToken<ArrayList<Contact>>() {
            }.getType();
            ArrayList<Contact> list = new Gson().fromJson(result, listType);
            requests = list;
        }
        return requests;
    }

    public synchronized void saveFriendReq(Context context, ArrayList<Contact> requests) {
        Type listType = new TypeToken<ArrayList<Contact>>() {
        }.getType();
        String reqStr = new Gson().toJson(requests, listType);

        SharedPreferences sp = context.getApplicationContext().getSharedPreferences("FriendReq", 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("requests", reqStr);
        editor.apply();
    }


    public synchronized ArrayList<ChatSession> getChatSessions(Context context) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences("Session", 0);
        Map<String, ?> results = sp.getAll();
        ArrayList<ChatSession> sessions = new ArrayList<>();
        for (Map.Entry<String, ?> e : results.entrySet()) {
            ChatSession session = new Gson().fromJson(e.getValue().toString(), ChatSession.class);
            sessions.add(session);
        }
        return sessions;
    }

    public synchronized ChatSession getChatSessionById(Context context, String uid) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences("Session", 0);
        String result = sp.getString(uid, "");
        if (!result.isEmpty()) {
            ChatSession session = new Gson().fromJson(result, ChatSession.class);
            return session;
        }
        return null;
    }

    public synchronized void appendEnvelopToChatSession(Context context, Contact contact,Envelop envelop) {
        ChatSession session = getChatSessionById(context, contact.uid);
        if (session == null) {
            session = new ChatSession(contact);
        }
        session.addEnvelop(envelop);
        addChatSessions(context, session);
    }

    public synchronized void deleteMostRecentEnvelopFromChatSession(Context context, String sender) {
        ChatSession session = getChatSessionById(context, sender);
        if (session == null) {
            return;
        }
        for (int i = session.chatHistory.size()-1;i>=0;i--) {
            if (session.chatHistory.get(i).from.uid.equals(sender)) {
                session.chatHistory.remove(i);
                break;
            }
        }
        addChatSessions(context, session);
    }

    public synchronized void addChatSessions(Context context, ChatSession session) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences("Session", 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(session.contact.uid, new Gson().toJson(session));
        editor.apply();
    }

    public synchronized void delChatSession(Context context, ChatSession session) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences("Session", 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(session.contact.uid);
        editor.apply();
    }

    public synchronized void delAllSessions(Context context) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences("Session", 0);
        SharedPreferences.Editor editor = sp.edit();
        Set<String> keys = sp.getAll().keySet();
        for (String k : keys) {
            editor.remove(k);
        }
        editor.apply();
    }

    @Deprecated
    public synchronized void saveAllSessions(Context context, ArrayList<ChatSession> sessions) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences("Session", 0);
        SharedPreferences.Editor editor = sp.edit();
        for (ChatSession s : sessions) {
            editor.putString(s.contact.uid, new Gson().toJson(s));
        }
        editor.apply();
    }

    public synchronized AppUser getUser(Context context) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences("AppUser", 0);
        AppUser user = new AppUser(
                sp.getString("uid", ""),
                sp.getString("email", ""),
                sp.getString("nick", ""),
                sp.getString("mobile", ""),
                sp.getBoolean("login", false)); // not in use
        return user;
    }

    public synchronized void saveUser(Context context, AppUser user) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences("AppUser", 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("uid", user.uid);
        editor.putString("email", user.email);
        editor.putString("nick", user.nick);
        editor.putString("mobile", user.mobile);
        editor.putBoolean("login", true);
        editor.apply();
    }

    public synchronized void updateNick(Context context, String nickname) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences("AppUser", 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("nick", nickname);
        editor.apply();
    }

    public synchronized void login(Context context) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences("Account", 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("isLogin", true);
        editor.apply();
    }

    public synchronized void logout(Context context) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences("Account", 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("isLogin", false);
        editor.apply();
    }

    public synchronized boolean isLoggedIn(Context context) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences("Account", 0);
        Boolean isLogin = sp.getBoolean("isLogin", false);
        return isLogin;
    }
}
