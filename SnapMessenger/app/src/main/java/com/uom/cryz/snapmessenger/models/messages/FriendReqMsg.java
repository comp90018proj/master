package com.uom.cryz.snapmessenger.models.messages;

/**
 * Created by ruijiankun on 5/10/2016.
 */
public class FriendReqMsg extends JsonMsg {
    public static final String _command = "FRIEND_REQ";
    public String senderNick = "";
    public FriendReqMsg(String sender, String receiver, String senderNick) {
        this.command = _command;
        this.sender = sender;
        this.receiver = receiver;
        this.senderNick = senderNick;
    }
}
