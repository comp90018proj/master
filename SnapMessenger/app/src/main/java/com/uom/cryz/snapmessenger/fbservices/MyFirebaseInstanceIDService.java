package com.uom.cryz.snapmessenger.fbservices;

import android.accounts.Account;
import android.app.Application;
import android.provider.ContactsContract;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.uom.cryz.snapmessenger.MainActivity;
import com.uom.cryz.snapmessenger.controllers.AccountController;
import com.uom.cryz.snapmessenger.controllers.DataController;
import com.uom.cryz.snapmessenger.controllers.MyVolley;

/**
 * Created by ruijiankun on 2/10/2016.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseInsIDService";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "New Token: " + refreshedToken);

        /*
        if (DataController.getInstance().isLoggedIn(MainActivity.getAppContext())) {
            if (MainActivity.getAppContext() != null) {
                AccountController.getInstance().updateToken(callback, MainActivity.getAppContext(),
                        DataController.getInstance().getUser(MainActivity.getAppContext()).uid,
                        FirebaseInstanceId.getInstance().getToken()
                );
            }
        }
        */
    }

    MyVolley.VolleyCallback callback = new MyVolley.VolleyCallback() {
        @Override
        public void onSuccess(Object result) {
        }
    };
}
