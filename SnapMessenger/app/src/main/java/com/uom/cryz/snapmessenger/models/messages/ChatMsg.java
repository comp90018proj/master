package com.uom.cryz.snapmessenger.models.messages;

/**
 * Created by ruijiankun on 5/10/2016.
 */
public class ChatMsg extends JsonMsg {
    public enum MsgType {
        TEXT,
        IMAGE,
        VIDEO,
        WITHDRAW
    }

    public static final String _command = "CHAT";
    public MsgType msgType = MsgType.TEXT;
    public String msgStr = "";
    public long msgLife = 20;

    public ChatMsg(String sender, String receiver, MsgType type, String content, long life) {
        this.command = _command;
        this.sender = sender;
        this.receiver = receiver;
        this.msgType = type;
        this.msgStr = content;
        this.msgLife = life;
    }

    public ChatMsg(String sender, String receiver, MsgType type, String content) {
        this.command = _command;
        this.sender = sender;
        this.receiver = receiver;
        this.msgType = type;
        this.msgStr = content;
    }

}
