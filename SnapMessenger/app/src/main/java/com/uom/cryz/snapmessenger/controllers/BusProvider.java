package com.uom.cryz.snapmessenger.controllers;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by ruijiankun on 13/10/2016.
 */
public class BusProvider {

    private static final Bus BUS = new Bus(ThreadEnforcer.ANY);

    public static Bus getInstance() {return BUS;}

    private BusProvider () {

    }
}
