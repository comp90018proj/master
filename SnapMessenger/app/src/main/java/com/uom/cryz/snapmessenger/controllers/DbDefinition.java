package com.uom.cryz.snapmessenger.controllers;


import android.provider.BaseColumns;

/**
 * Created by ruijiankun on 5/10/2016.
 */
@Deprecated
public final class DbDefinition {

    private DbDefinition() {}

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    //TABLE - message sent

    public static class MsgSentEntry implements BaseColumns {
        public static final String TABLE_NAME = "msg_sent";
        public static final String COLUMN_NAME_RECEIVER = "receiver";
        public static final String COLUMN_NAME_SENT_TIME = "sent_time";
        public static final String COLUMN_NAME_LIFE_SPAN = "life_span";
        public static final String COLUMN_NAME_MSG_TYPE = "msg_type";
        public static final String COLUMN_NAME_MSG_CONTENT = "msg_content";
    }

    public static final String SQL_CREATE_MSGSENT =
            "CREATE TABLE " + MsgSentEntry.TABLE_NAME + " (" +
                    MsgSentEntry._ID + " INTEGER PRIMARY KEY," +
                    MsgSentEntry.COLUMN_NAME_RECEIVER + TEXT_TYPE + COMMA_SEP +
                    MsgSentEntry.COLUMN_NAME_SENT_TIME + TEXT_TYPE + COMMA_SEP +
                    MsgSentEntry.COLUMN_NAME_LIFE_SPAN + TEXT_TYPE + COMMA_SEP +
                    MsgSentEntry.COLUMN_NAME_MSG_TYPE + TEXT_TYPE + COMMA_SEP +
                    MsgSentEntry.COLUMN_NAME_MSG_CONTENT + TEXT_TYPE + " )";

    public static final String SQL_DELETE_MSGSENT =
            "DROP TABLE IF EXISTS " + MsgSentEntry.TABLE_NAME;

    //TABLE - message received

    public static class MsgReceivedEntry implements BaseColumns {
        public static final String TABLE_NAME = "msg_received";
        public static final String COLUMN_NAME_SENDER = "sender";
        public static final String COLUMN_NAME_RECEIVE_TIME = "receive_time";
        public static final String COLUMN_NAME_OPEN_TIME = "open_time";
        public static final String COLUMN_NAME_LIFE_SPAN = "life_span";
        public static final String COLUMN_NAME_MSG_TYPE = "msg_type";
        public static final String COLUMN_NAME_MSG_CONTENT = "msg_content";
    }

    public static final String SQL_CREATE_MSGRECEIVED =
            "CREATE TABLE " + MsgReceivedEntry.TABLE_NAME + " (" +
                    MsgReceivedEntry._ID + " INTEGER PRIMARY KEY," +
                    MsgReceivedEntry.COLUMN_NAME_SENDER + TEXT_TYPE + COMMA_SEP +
                    MsgReceivedEntry.COLUMN_NAME_RECEIVE_TIME + TEXT_TYPE + COMMA_SEP +
                    MsgReceivedEntry.COLUMN_NAME_OPEN_TIME + TEXT_TYPE + COMMA_SEP +
                    MsgReceivedEntry.COLUMN_NAME_LIFE_SPAN + TEXT_TYPE + COMMA_SEP +
                    MsgReceivedEntry.COLUMN_NAME_MSG_TYPE + TEXT_TYPE + COMMA_SEP +
                    MsgReceivedEntry.COLUMN_NAME_MSG_CONTENT + TEXT_TYPE + " )";

    public static final String SQL_DELETE_MSGRECEIVED =
            "DROP TABLE IF EXISTS " + MsgReceivedEntry.TABLE_NAME;


}
