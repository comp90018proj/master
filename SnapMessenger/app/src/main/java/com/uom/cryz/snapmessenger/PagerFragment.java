package com.uom.cryz.snapmessenger;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by zzw on 10/14/2016.
 */
public class PagerFragment extends Fragment {
    private ViewPager pager;
    private View view;
    public PagerFragment() {
        // Required empty public constructor
    }

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pager, container, false);
        pager = (ViewPager) view.findViewById(R.id.vpager);

        pager.setAdapter(buildAdapter());
        pager.setCurrentItem(1);
        pager.setOnPageChangeListener(new OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {}
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            public void onPageSelected(int position) {
                switch(position){
                    case 0: MainActivity.hpager.setPagingEnabled(false);break;
                    case 1: MainActivity.hpager.setPagingEnabled(true);break;
                    case 2: MainActivity.hpager.setPagingEnabled(false);break;
                }
            }
        });
        return (view);
    }

    private PagerAdapter buildAdapter() {
        return(new SampleAdapter(getActivity(), getChildFragmentManager()));
    }
    public class SampleAdapter extends FragmentPagerAdapter {
        Context ctxt=null;

        public SampleAdapter(Context ctxt, FragmentManager mgr) {
            super(mgr);
            this.ctxt=ctxt;
        }

        @Override
        public int getCount() {
            return(3);
        }

        @Override
        public Fragment getItem(int position) {
            CameraFragment tempFragment = new CameraFragment();
            switch(position){
                case 0: return new ProfileFragment();
                case 1: return tempFragment;
                case 2: return new MemoryFragment();
                default: return tempFragment;
            }
        }

    }
}
