package com.uom.cryz.snapmessenger.controllers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ruijiankun on 5/10/2016.
 */
@Deprecated
public class DbHelper extends SQLiteOpenHelper{
    public static final int DATABSE_VERSION = 1;
    public static final String DATABASE_NAME = "UserData.db";

    public DbHelper(Context context)
    {
        super(context,DATABASE_NAME,null,DATABSE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DbDefinition.SQL_CREATE_MSGRECEIVED);
        db.execSQL(DbDefinition.SQL_CREATE_MSGSENT);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void resetDb(SQLiteDatabase db)
    {
        db.execSQL(DbDefinition.SQL_DELETE_MSGRECEIVED);
        db.execSQL(DbDefinition.SQL_DELETE_MSGSENT);
        onCreate(db);
    }
}
