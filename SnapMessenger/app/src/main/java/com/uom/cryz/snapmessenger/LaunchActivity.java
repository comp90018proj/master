package com.uom.cryz.snapmessenger;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.google.firebase.iid.FirebaseInstanceId;
import com.uom.cryz.snapmessenger.controllers.AccountController;
import com.uom.cryz.snapmessenger.controllers.DataController;
import com.uom.cryz.snapmessenger.controllers.MyVolley;

public class LaunchActivity extends AppCompatActivity {
    FragmentManager manager = getSupportFragmentManager();

    public void loginBtnOnClick(View view) {
        LoginFragment loginFragment = new LoginFragment();
        manager.beginTransaction().replace(R.id.firstPage, loginFragment, loginFragment.getTag()).commit();
    }

    public void registerBtnOnClick(View view) {
        RegisterFragment registerFragment = new RegisterFragment();
        manager.beginTransaction().replace(R.id.firstPage, registerFragment, registerFragment.getTag()).commit();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        StartFragment startFragment = new StartFragment();
        manager.beginTransaction().replace(R.id.firstPage, startFragment, startFragment.getTag()).commit();

    }

        @Override
        protected void onStart () {
            super.onStart();
            if (DataController.getInstance().isLoggedIn(getApplicationContext())) {
                AccountController.getInstance().updateToken(
                        new MyVolley.VolleyCallback() {
                            @Override
                            public void onSuccess(Object result) {
                            }
                        },
                        getApplicationContext(),
                        DataController.getInstance().getUser(getApplicationContext()).uid,
                        FirebaseInstanceId.getInstance().getToken()
                );
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
            }
        }

        @Override
        protected void onStop () {
            super.onStop();
            this.finish();
        }
    }


