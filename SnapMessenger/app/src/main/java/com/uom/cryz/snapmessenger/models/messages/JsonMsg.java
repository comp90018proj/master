package com.uom.cryz.snapmessenger.models.messages;

import com.google.gson.Gson;

/**
 * Created by ruijiankun on 5/10/2016.
 */
public class JsonMsg {
    public String command = "";
    public String sender = "";
    public String receiver = "";

    public String toJsonString() {
        return new Gson().toJson(this);
    }
}
