package com.uom.cryz.snapmessenger.models;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ruijiankun on 4/10/2016.
 */
public class ChatSession {
    public Contact contact = null;
    public ArrayList<Envelop> chatHistory;
    public ChatSession(Contact contact)
    {
        this.contact = contact;
        chatHistory = new ArrayList<Envelop>();
    }
    public ChatSession(Contact contact, ArrayList<Envelop> chatHistory)
    {
        this.contact = contact;
        this.chatHistory =chatHistory;
    }

    public synchronized void deleteEnvelop(Envelop envelop){
        chatHistory.remove(envelop);
    }
    public synchronized void addEnvelop(Envelop envelop){
        chatHistory.add(envelop);
    }
}
