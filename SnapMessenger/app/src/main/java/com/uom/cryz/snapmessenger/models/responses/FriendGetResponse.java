package com.uom.cryz.snapmessenger.models.responses;

import com.uom.cryz.snapmessenger.models.Contact;

import java.util.ArrayList;

/**
 * Created by ruijiankun on 8/10/2016.
 */
public class FriendGetResponse extends ServerResponse{
    public ArrayList<Contact> contacts;
    public ArrayList<Contact> requests;
    public FriendGetResponse(Boolean success)
    {
        super(success);
    }
 }
