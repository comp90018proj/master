package com.uom.cryz.snapmessenger.controllers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by ruijiankun on 12/10/2016.
 */
public class StorageController {
    private static StorageController storageController = null;

    public static synchronized StorageController getInstance() {
        if (storageController == null) {
            storageController = new StorageController();
        }
        return storageController;
    }
    public StorageController() {
    }

    public Bitmap loadImage(String filePath) throws FileNotFoundException {

        Bitmap bitmap;
        File f = new File(filePath);
        bitmap = BitmapFactory.decodeStream(new FileInputStream(f));
        return bitmap;
    }
}
