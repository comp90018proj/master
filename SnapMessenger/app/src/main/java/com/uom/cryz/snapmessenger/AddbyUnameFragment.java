package com.uom.cryz.snapmessenger;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.uom.cryz.snapmessenger.controllers.DataController;
import com.uom.cryz.snapmessenger.controllers.FriendController;
import com.uom.cryz.snapmessenger.controllers.MyVolley;
import com.uom.cryz.snapmessenger.models.Envelop;
import com.uom.cryz.snapmessenger.models.messages.FriendReqMsg;
import com.uom.cryz.snapmessenger.models.responses.FriendSearchResponse;
import com.uom.cryz.snapmessenger.models.responses.ServerResponse;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddbyUnameFragment extends Fragment {
    private static final String TAG = AddbyUnameFragment.class.getSimpleName();


    public AddbyUnameFragment() {
        // Required empty public constructor

    }

    View myView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_addby_uname, container, false);
        assignViewActions();
        switchVisibility(false);
        return myView;
    }


    MyVolley.VolleyCallback searchCallback = new MyVolley.VolleyCallback() {
        @Override
        public void onSuccess(Object result) {
            FriendSearchResponse resp = (FriendSearchResponse) result;
            if (resp.success) {
                TextView txt = (TextView) myView.findViewById(R.id.txt_add_by_uname_resultname);
                txt.setText(String.format("%s", resp.contact.uid, resp.contact.nick));
                switchVisibility(true);
            } else {
                switchVisibility(false);
            }
        }
    };

    MyVolley.VolleyCallback addCallback = new MyVolley.VolleyCallback() {
        @Override
        public void onSuccess(Object result) {
            ServerResponse resp = (ServerResponse) result;
            if (resp.success) {
                Toast.makeText(getActivity().getApplicationContext(), "Request has been sent!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity().getApplicationContext(), "Request failed!", Toast.LENGTH_SHORT).show();
            }
        }
    };


    private void assignViewActions() {
        ImageView imgBack = (ImageView) myView.findViewById(R.id.imgview_addby_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment addFriendsFragment = new AddFriendFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(
                        R.id.layout_content_main,
                        addFriendsFragment,
                        addFriendsFragment.getTag()
                ).commit();
            }
        });

        // search result
        final EditText editSearch = (EditText) myView.findViewById(R.id.edit_add_by_name);
        editSearch.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    FriendController.getInstance().searchFriend(
                            searchCallback,
                            getActivity().getApplicationContext(),
                            editSearch.getText().toString().trim());
                }
                return false;
            }

        });

        ImageView imgSearch = (ImageView) myView.findViewById(R.id.imgview_addby_name_search);
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FriendController.getInstance().searchFriend(
                        searchCallback,
                        getActivity().getApplicationContext(),
                        editSearch.getText().toString().trim());
            }
        });

        final TextView txtResult = (TextView) myView.findViewById(R.id.txt_add_by_uname_resultname);
        final ImageView imgAdd = (ImageView) myView.findViewById(R.id.imgview_addby_name_add);
        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!txtResult.getText().toString().trim().isEmpty()) {
                    String uid = DataController.getInstance().getUser(getActivity().getApplicationContext()).uid;
                    String nick = DataController.getInstance().getUser(getActivity().getApplicationContext()).nick;
                    Toast.makeText(getActivity().getApplicationContext(),  txtResult.getText().toString().trim(), Toast.LENGTH_SHORT).show();
                    FriendController.getInstance().addById(
                            addCallback,
                            getActivity().getApplicationContext(),
                            uid,
                            txtResult.getText().toString(),
                            new FriendReqMsg(uid, txtResult.getText().toString().trim(), nick)
                    );

                    imgAdd.setImageResource(R.drawable.btn_add_pressed);
                }
            }
        });
    }

    /**
     * If there is results, show results. If not, show not found
     * @param resultVisible
     */
    private void switchVisibility(boolean resultVisible) {
        if (resultVisible) {
            ImageView img = (ImageView) myView.findViewById(R.id.imgview_addby_name_pic);
            img.setVisibility(View.VISIBLE);

            TextView txt = (TextView) myView.findViewById(R.id.txt_add_by_uname_resultname);
            txt.setVisibility(View.VISIBLE);

            ImageView btn = (ImageView) myView.findViewById(R.id.imgview_addby_name_add);
            btn.setVisibility(View.VISIBLE);
            btn.setImageResource(R.drawable.btn_add);

            TextView tip = (TextView) myView.findViewById(R.id.txt_add_by_uname_tip);
            tip.setVisibility(View.GONE);
        } else {
            ImageView img = (ImageView) myView.findViewById(R.id.imgview_addby_name_pic);
            img.setVisibility(View.GONE);

            TextView txt = (TextView) myView.findViewById(R.id.txt_add_by_uname_resultname);
            txt.setVisibility(View.GONE);
            txt.setText("");

            ImageView btn = (ImageView) myView.findViewById(R.id.imgview_addby_name_add);
            btn.setVisibility(View.GONE);

            TextView tip = (TextView) myView.findViewById(R.id.txt_add_by_uname_tip);
            tip.setVisibility(View.VISIBLE);
        }
    }
}