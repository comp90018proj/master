package com.uom.cryz.snapmessenger;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.util.AsyncListUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.uom.cryz.snapmessenger.controllers.DataController;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddFriendFragment extends Fragment {

    private static final String TAG = AddFriendFragment.class.getSimpleName();
    public AddFriendFragment() {
        // Required empty public constructor
    }

    View myView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_add_friend, container, false);
        assignViewActions();
        return myView;
    }


    private void assignViewActions() {
        ImageView imgBack = (ImageView) myView.findViewById(R.id.imgview_add_friend_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment emptyFragment = new EmptyFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(
                        R.id.layout_content_main,
                        emptyFragment
                ).commit();
                ViewPager pager = (ViewPager) getActivity().findViewById(R.id.pager);
                pager.setVisibility(View.VISIBLE);
            }
        });



        TextView txtAddbyUname = (TextView) myView.findViewById(R.id.txt_add_by_uname);
        txtAddbyUname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment addbyUnameFragment = new AddbyUnameFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(
                        R.id.layout_content_main,
                        addbyUnameFragment,
                        addbyUnameFragment.getTag()
                ).commit();
            }
        });

        TextView txtSnapCode = (TextView) myView.findViewById(R.id.txt_add_by_snapcode);
        txtSnapCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment addbySnapCode = new QRcodeFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(
                        R.id.layout_content_main,
                        addbySnapCode,
                        addbySnapCode.getTag()
                ).commit();
            }
        });


        TextView txtSharrUname = (TextView) myView.findViewById(R.id.txt_share_uname);
        txtSharrUname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String shareBody = String.format(
                        "Add me on SnapMessenger. My user name is '%s'",
                        DataController.getInstance().getUser(getActivity().getApplicationContext()).uid);
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share using"));
            }
        });


    }

}
