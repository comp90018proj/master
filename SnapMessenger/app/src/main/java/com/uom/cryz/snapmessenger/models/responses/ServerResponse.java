package com.uom.cryz.snapmessenger.models.responses;

/**
 * Created by ruijiankun on 8/10/2016.
 */
public class ServerResponse {
    public Boolean success;

    public ServerResponse(Boolean success) {
        this.success = success;
    }
}
