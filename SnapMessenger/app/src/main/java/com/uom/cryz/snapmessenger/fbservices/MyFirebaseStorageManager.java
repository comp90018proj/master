package com.uom.cryz.snapmessenger.fbservices;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by ruijiankun on 10/10/2016.
 */
public class MyFirebaseStorageManager {
    public static final String STORAGE_PATH = "gs://mobile-computing-proj.appspot.com/";
    public static final String IMAGE_PATH = "image/";
    public static final String VIDEO_PATH = "video/";

    private static MyFirebaseStorageManager manager;
    FirebaseStorage storage;

    public static MyFirebaseStorageManager getInstance() {
        if (manager == null) {
            manager = new MyFirebaseStorageManager();
        }
        return manager;
    }


    public MyFirebaseStorageManager() {
        storage = FirebaseStorage.getInstance();
    }

    public void uploadFile(OnSuccessListener<UploadTask.TaskSnapshot> listener, StorageReference ref, byte[] data) {
        UploadTask uploadTask = ref.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                exception.printStackTrace();
            }
        }).addOnSuccessListener(listener);
    }

    public void downloadFile(OnSuccessListener<FileDownloadTask.TaskSnapshot> listener, StorageReference ref, File file) throws IOException {

        ref.getFile(file).addOnSuccessListener(listener).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });
    }


}
