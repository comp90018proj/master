package com.uom.cryz.snapmessenger.models.messages;

/**
 * Created by ruijiankun on 5/10/2016.
 */
public class FriendAckMsg extends JsonMsg {
    public static final String _command = "FRIEND_ACK";
    public boolean ack = false;
    public String senderNick = "";

    public FriendAckMsg(boolean ack, String sender, String receiver, String senderNick) {
        this.command = _command;
        this.ack = ack;
        this.sender = sender;
        this.receiver = receiver;
        this.senderNick = senderNick;
    }
}
