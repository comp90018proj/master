package com.uom.cryz.snapmessenger;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.uom.cryz.snapmessenger.controllers.AccountController;
import com.uom.cryz.snapmessenger.controllers.DataController;
import com.uom.cryz.snapmessenger.controllers.FriendController;
import com.uom.cryz.snapmessenger.controllers.GeoInfoController;
import com.uom.cryz.snapmessenger.controllers.MyVolley;
import com.uom.cryz.snapmessenger.models.responses.FriendGetResponse;
import com.uom.cryz.snapmessenger.models.responses.ServerResponse;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    public static final String TAG = ProfileFragment.class.getSimpleName();
    View myView;
    public ProfileFragment() {
        // Required empty public constructor
    }

    private void refresh() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(this);
        ft.attach(this);
        ft.commit();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.fragment_profile, container, false);
        assignViewActions();
        GeoInfoController.getInstance(getActivity().getApplicationContext()).trigger(getActivity().getApplicationContext());

        //set nick name TextView
        final String nickName = DataController.getInstance().getUser(getActivity().getApplicationContext()).nick;
        final String uid = DataController.getInstance().getUser(getActivity().getApplicationContext()).uid;
        TextView nicknameTV = (TextView) myView.findViewById(R.id.txt_nickname);
        TextView uidTV = (TextView) myView.findViewById(R.id.txt_uid);
        nicknameTV.setText(nickName);
        uidTV.setText(uid);
        ImageView qrcode = (ImageView) myView.findViewById(R.id.qrcode);

        Bitmap bm = generateQRCode(uid);

        if(bm != null) {
            qrcode.setImageBitmap(bm);
        }

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.nickname_dialog_layout);
                dialog.setTitle("Change Nickname");
                TextView tv1 = (TextView) dialog.findViewById(R.id.dialogTV1);
                TextView tv2 = (TextView) dialog.findViewById(R.id.dialogTV2);
                final EditText et = (EditText) dialog.findViewById(R.id.dialogET);
                Button confirmBtn = (Button) dialog.findViewById(R.id.dialogConfirmBtn);
                Button cancelBtn = (Button) dialog.findViewById(R.id.dialogCancelBtn);
                tv1.setText("Your current nickname is: " + nickName);
                tv2.setText("Please enter your new nickname: ");
                confirmBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String inputNick = et.getText().toString();
                        if (!inputNick.equals(nickName)) {
                            //update nickname
                            AccountController.getInstance().updateNick(new MyVolley.VolleyCallback() {
                                @Override
                                public void onSuccess(Object result) {
                                    ServerResponse resp = (ServerResponse) result;
                                    if (resp.success) {
                                        DataController.getInstance().updateNick(getActivity().getApplicationContext(), inputNick);
                                        refresh();
                                    }

                                }
                            }, getActivity(), uid, inputNick);

                            Toast.makeText(getActivity().getApplicationContext(), "Change Nickname Success!", Toast.LENGTH_SHORT).show();

                            dialog.dismiss();
                        } else {
                            Toast.makeText(getActivity().getApplicationContext(), "Make sure you entered a new nickname!", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();


            }
        };
        nicknameTV.setOnClickListener(listener);
        uidTV.setOnClickListener(listener);



        return myView;
    }


    private void assignViewActions() {
        TextView txtAddMe = (TextView) myView.findViewById(R.id.txt_add_me);
        txtAddMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FriendController.getInstance().syncFriends(
                        addMeCallback,
                        MainActivity.getAppContext(),
                        DataController.getInstance().getUser(MainActivity.getAppContext()).uid);

            }
        });

        TextView txtAddFriends = (TextView) myView.findViewById(R.id.txt_add_friends);
        txtAddFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment addFriendFragment = new AddFriendFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(
                        R.id.layout_content_main,
                        addFriendFragment,
                        addFriendFragment.getTag()
                ).commit();
                ViewPager pager = (ViewPager)getActivity().findViewById(R.id.pager);
                pager.setVisibility(View.GONE);
            }
        });

        TextView txtNearBy = (TextView) myView.findViewById(R.id.tinder);
        txtNearBy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment addNearBy = new TinderFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(
                        R.id.layout_content_main,
                        addNearBy,
                        addNearBy.getTag()
                ).commit();
                ViewPager pager = (ViewPager)getActivity().findViewById(R.id.pager);
                pager.setVisibility(View.GONE);
            }
        });

        TextView txtMyFriends = (TextView) myView.findViewById(R.id.txt_my_friends);
        txtMyFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FriendController.getInstance().syncFriends(
                        myFriendCallback,
                        MainActivity.getAppContext(),
                        DataController.getInstance().getUser(MainActivity.getAppContext()).uid);
            }
        });

        ImageView logoutImg = (ImageView) myView.findViewById(R.id.imgview_prof_logout);
        logoutImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                AccountController.getInstance().logout(
                                        logoutCallback,
                                        getActivity().getApplicationContext(),
                                        DataController.getInstance().getUser(getActivity().getApplicationContext()).uid
                                );
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                dialog.dismiss();
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Are you sure to log out?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();





            }
        });
    }

    MyVolley.VolleyCallback addMeCallback = new MyVolley.VolleyCallback() {
        @Override
        public void onSuccess(Object result) {
            FriendGetResponse resp = (FriendGetResponse) result;
            if (resp.success) {
                DataController.getInstance().saveFriendReq(getActivity().getApplicationContext(), resp.requests);

                Fragment addMeFragment = new AddMeFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(
                        R.id.layout_content_main,
                        addMeFragment,
                        addMeFragment.getTag()
                ).commit();
                ViewPager pager = (ViewPager) getActivity().findViewById(R.id.pager);
                pager.setVisibility(View.GONE);
            }
        }
    };


    MyVolley.VolleyCallback myFriendCallback = new MyVolley.VolleyCallback() {
        @Override
        public void onSuccess(Object result) {
            FriendGetResponse resp = (FriendGetResponse) result;
            if (resp.success) {
                DataController.getInstance().saveContacts(getActivity().getApplicationContext(), resp.contacts);


                Fragment myFriendFragment = new MyFriendsFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                Bundle b = new Bundle();
                b.putString("fromPage", TAG);
                myFriendFragment.setArguments(b);
                fm.beginTransaction().replace(
                        R.id.layout_content_main,
                        myFriendFragment,
                        myFriendFragment.getTag()
                ).commit();
                ViewPager pager = (ViewPager) getActivity().findViewById(R.id.pager);
                pager.setVisibility(View.GONE);
            }
        }
    };

    MyVolley.VolleyCallback logoutCallback = new MyVolley.VolleyCallback() {
        @Override
        public void onSuccess(Object result) {
            ServerResponse resp = (ServerResponse) result;
            if(resp.success) {
                DataController.getInstance().resetUserData(getActivity().getApplicationContext());
                DataController.getInstance().logout(getActivity().getApplicationContext());
                Log.d(TAG,"Logout");
                Toast.makeText(getActivity().getApplicationContext(),"You have logged out! ", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getActivity().getApplicationContext(), LaunchActivity.class);
                startActivity(i);
            } else {
                Toast.makeText(getActivity().getApplicationContext(),"Logout Failed. Please try again later.", Toast.LENGTH_LONG).show();
            }
        }
    };

    private Bitmap generateQRCode(String data) {
        com.google.zxing.Writer writer = new QRCodeWriter();
        String finaldata = Uri.encode(data, "ISO-8859-1");
        Bitmap mBitmap = null;
        try {
            BitMatrix bm = writer.encode(finaldata,BarcodeFormat.QR_CODE, 350, 350);
            mBitmap = Bitmap.createBitmap(350, 350, Bitmap.Config.ARGB_8888);
            for (int i = 0; i < 350; i++) {
                for (int j = 0; j < 350; j++) {
                    mBitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK: Color.WHITE);
                }
            }
        } catch (WriterException e) {
            e.printStackTrace();
        }
        if (mBitmap != null) {
            return mBitmap;
        }
        return null;
    }
}
