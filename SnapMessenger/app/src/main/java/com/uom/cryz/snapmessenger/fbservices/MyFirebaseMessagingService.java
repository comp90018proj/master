package com.uom.cryz.snapmessenger.fbservices;

import  com.uom.cryz.snapmessenger.MainActivity;
import  com.uom.cryz.snapmessenger.R;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.uom.cryz.snapmessenger.controllers.MessageHandler;


/**
 * Created by ruijiankun on 2/10/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "FROM: " + remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Msg Data: " + remoteMessage.getData());
            // put all msg in json
            String jsonData = remoteMessage.getData().get("json");
            if(jsonData !=null)
            {
                MessageHandler.getInstance().handleFirebaseMsg(jsonData);
            }
            //TODO: else ?
        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Msg Body: " + remoteMessage.getNotification().getBody());
            sendNotification(remoteMessage.getNotification().getBody());
        }
    }

    //display
    private void sendNotification(String body) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0/*req code?*/, intent, PendingIntent.FLAG_ONE_SHOT);

        //set sound of notification
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notifiBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("SnapMessenger")
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(sound)
                .setContentIntent(pendingIntent);
        NotificationManager notificationMan = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationMan.notify(0/*ID of notification*/,notifiBuilder.build());
    }
}
