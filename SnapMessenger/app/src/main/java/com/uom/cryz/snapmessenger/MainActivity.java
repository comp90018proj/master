package com.uom.cryz.snapmessenger;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.uom.cryz.snapmessenger.controllers.DataController;
import com.uom.cryz.snapmessenger.controllers.FriendController;
import com.uom.cryz.snapmessenger.controllers.MyVolley;
import com.uom.cryz.snapmessenger.models.messages.FriendReqMsg;
import com.uom.cryz.snapmessenger.models.responses.ServerResponse;

import java.util.ArrayList;

public class MainActivity extends FragmentActivity  {

    //view pager
    protected static CustomViewPager  hpager;
    private PagerAdapter hPagerAdapter;

    private static final String TAG = "MainActivity";

    private  static  Context context;
    public static Context getAppContext()
    {
        return  context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setContentView(R.layout.content_main);
        //TODO: initialize
        context = this.getApplicationContext();

        hpager = (CustomViewPager) findViewById(R.id.pager);
        hPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        hpager.setAdapter(hPagerAdapter);
       // DataController.getInstance(this.getApplicationContext()).resetUserDb();
        hpager.setCurrentItem(1);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    public void onBackPressed() {
        if (hpager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            hpager.setCurrentItem(hpager.getCurrentItem() - 1);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null || data == null || data.getStringExtra("SCAN_RESULT") == null) {
                Log.d("MainActivity","Cancelled scan");
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();

            }else{
                Log.d("MainActivity","scanned");
                Toast.makeText(this, "Scanned", Toast.LENGTH_LONG).show();
                String contents = data.getStringExtra("SCAN_RESULT");
                String uid = DataController.getInstance().getUser(this.getApplicationContext()).uid;
                String nick = DataController.getInstance().getUser(this.getApplicationContext()).nick;
                contents = contents.replace("%40","@");
                Toast.makeText(getApplicationContext(), contents, Toast.LENGTH_SHORT).show();
                FriendController.getInstance().addById(
                        addCallback,
                        this.getApplicationContext(),
                        uid,
                        contents,
                        new FriendReqMsg(uid, contents, nick));
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    MyVolley.VolleyCallback addCallback = new MyVolley.VolleyCallback() {
        @Override
        public void onSuccess(Object result) {
            ServerResponse resp = (ServerResponse) result;
            if (resp.success) {
                Toast.makeText(getApplicationContext(), "Request has been sent!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Request failed!", Toast.LENGTH_SHORT).show();
            }
        }
    };

}
