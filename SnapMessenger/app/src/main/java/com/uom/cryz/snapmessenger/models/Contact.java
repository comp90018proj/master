package com.uom.cryz.snapmessenger.models;

/**
 * Created by ruijiankun on 4/10/2016.
 */
public class Contact {
    public String uid;
    public String email;
    public String nick;
    public String remark;
    public String mobile;
    public String token;
    public int blocked = 0;

    public Contact(String id, String email, String nick, String remark, String mobile, String token) {
        this.uid = id;
        this.email = email;
        this.nick = nick;
        this.remark = remark;
        this.mobile = mobile;
        this.token = token;
    }
}
