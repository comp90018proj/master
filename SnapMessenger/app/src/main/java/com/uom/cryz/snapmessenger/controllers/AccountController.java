package com.uom.cryz.snapmessenger.controllers;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.uom.cryz.snapmessenger.util.NetworkChecker;
import com.uom.cryz.snapmessenger.util.SHA1Encoder;
import com.uom.cryz.snapmessenger.models.responses.LoginResponse;
import com.uom.cryz.snapmessenger.models.responses.RegisterResponse;
import com.uom.cryz.snapmessenger.models.responses.ServerResponse;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ruijiankun on 6/10/2016.
 *
 * Account Controller, manage user register, login, logout etc behaviours, communicate with server
 */
public class AccountController {
    private static final String TAG = AccountController.class.getSimpleName();
    private static final String LOGIN_URL = "http://jackrui.uicp.hk/comp90018/login.php";
    private static final String LOGOUT_URL = "http://jackrui.uicp.hk/comp90018/logout.php";
    private static final String REGISTER_URL = "http://jackrui.uicp.hk/comp90018/register.php";
    private static final String TOKEN_UP_URL = "http://jackrui.uicp.hk/comp90018/up_token.php";
    private static final String NICK_UP_URL = "http://jackrui.uicp.hk/comp90018/up_nick.php";

    private static AccountController controller;

    public static AccountController getInstance() {
        if (controller == null) {
            controller = new AccountController();
        }
        return controller;
    }

    public AccountController() {
    }

    private String hashPwd(String email, String pwd) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return SHA1Encoder.SHA1(email.trim() + pwd.trim());
    }

    public void login(final MyVolley.VolleyCallback callback, final Context context, final String email, String pwd) throws UnsupportedEncodingException, NoSuchAlgorithmException {

        if(!NetworkChecker.isNetworkOK(context))
        {
            Toast.makeText(context,"Network Error!", Toast.LENGTH_SHORT).show();
            callback.onSuccess(new LoginResponse(false));
            return;
        }

        final String pwdHash = hashPwd(email, pwd);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);

                        LoginResponse loginRes = new Gson().fromJson(response, LoginResponse.class);
                        callback.onSuccess(loginRes);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.getStackTrace().toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("email", email);
                headers.put("pwd", pwdHash);
                headers.put("token", FirebaseInstanceId.getInstance().getToken());
                Log.d(TAG,"TOKEN: "+FirebaseInstanceId.getInstance().getToken());
                return headers;
            }
        };

        MyVolley.getInstance(context).addToRequestQueue(stringRequest);
    }

    public void logout(final MyVolley.VolleyCallback callback, final Context context, final String uid) {
        if(!NetworkChecker.isNetworkOK(context))
        {
            Toast.makeText(context,"Network Error!", Toast.LENGTH_SHORT).show();
            callback.onSuccess(new ServerResponse(false));
            return;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGOUT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);

                        ServerResponse logoutRes = new Gson().fromJson(response, ServerResponse.class);
                        callback.onSuccess(logoutRes);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.getStackTrace().toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("uid", uid);
                return headers;
            }
        };
        MyVolley.getInstance(context).addToRequestQueue(stringRequest);
    }

    public void register(final MyVolley.VolleyCallback callback, final Context context, final String email, final String nick, String pwd, final String mobile) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        if(!NetworkChecker.isNetworkOK(context))
        {
            Toast.makeText(context,"Network Error!", Toast.LENGTH_SHORT).show();
            callback.onSuccess(new RegisterResponse(false));
            return;
        }

        final String pwdHash = hashPwd(email, pwd);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);

                        RegisterResponse regRes = new Gson().fromJson(response, RegisterResponse.class);

                        callback.onSuccess(regRes);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.getStackTrace().toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("email", email);
                headers.put("pwd", pwdHash);
                headers.put("nick", nick);
                headers.put("token", FirebaseInstanceId.getInstance().getToken());
                headers.put("mobile", mobile);
                return headers;
            }
        };

        MyVolley.getInstance(context).addToRequestQueue(stringRequest);
    }

    public void updateToken(final MyVolley.VolleyCallback callback, final Context context, String uid, String token) {
        if(!NetworkChecker.isNetworkOK(context))
        {
            Toast.makeText(context,"Network Error!", Toast.LENGTH_SHORT).show();
            callback.onSuccess(new ServerResponse(false));
            return;
        }

        final String sender = uid;
        final String tokenStr = token;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, TOKEN_UP_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG,response);
                        ServerResponse logoutRes = new Gson().fromJson(response, ServerResponse.class);
                        callback.onSuccess(logoutRes);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle error
                        Log.d(TAG,error.getStackTrace().toString());
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String,String>();
                headers.put("uid",sender);
                headers.put("token",tokenStr);
                return headers;
            }
        };

        MyVolley.getInstance(context).addToRequestQueue(stringRequest);
    }

    public void updateNick(final MyVolley.VolleyCallback callback, final Context context, final String uid, final String nick){
        if(!NetworkChecker.isNetworkOK(context))
        {
            Toast.makeText(context,"Network Error!", Toast.LENGTH_SHORT).show();
            callback.onSuccess(new ServerResponse(false));
            return;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, NICK_UP_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG,response);
                        ServerResponse resp = new Gson().fromJson(response, ServerResponse.class);
                        callback.onSuccess(resp);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle error
                        Log.d(TAG,error.getStackTrace().toString());
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String,String>();
                headers.put("uid",uid);
                headers.put("nick",nick);
                return headers;
            }
        };
        MyVolley.getInstance(context).addToRequestQueue(stringRequest);
    }
}
