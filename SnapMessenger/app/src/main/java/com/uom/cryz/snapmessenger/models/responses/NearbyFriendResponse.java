package com.uom.cryz.snapmessenger.models.responses;

import com.uom.cryz.snapmessenger.models.Contact;
import com.uom.cryz.snapmessenger.models.NearbyFriend;

import java.util.ArrayList;

/**
 * Created by ruijiankun on 18/10/2016.
 */
public class NearbyFriendResponse extends ServerResponse {
    public ArrayList<NearbyFriend> contacts;
    public NearbyFriendResponse(Boolean success) {
        super(success);
    }
}
