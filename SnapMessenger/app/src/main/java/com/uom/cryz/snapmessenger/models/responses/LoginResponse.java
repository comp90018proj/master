package com.uom.cryz.snapmessenger.models.responses;

/**
 * Created by ruijiankun on 8/10/2016.
 */
public class LoginResponse extends ServerResponse{
    public String uid;
    public String email;
    public String nick;
    public String mobile;
    public LoginResponse(Boolean success)
    {
        super(success);
    }
}
