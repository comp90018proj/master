package com.uom.cryz.snapmessenger;


import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.gson.Gson;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.uom.cryz.snapmessenger.controllers.BusProvider;
import com.uom.cryz.snapmessenger.controllers.DataController;
import com.uom.cryz.snapmessenger.controllers.MessageController;
import com.uom.cryz.snapmessenger.controllers.MyVolley;
import com.uom.cryz.snapmessenger.controllers.StorageController;
import com.uom.cryz.snapmessenger.models.ChatSession;
import com.uom.cryz.snapmessenger.models.Contact;
import com.uom.cryz.snapmessenger.models.Envelop;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

import com.uom.cryz.snapmessenger.models.messages.ChatMsg;
import com.uom.cryz.snapmessenger.models.responses.ServerResponse;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends Fragment {

    private static final String TAG = ChatFragment.class.getSimpleName();
    private static final int CODE_GALLERY_IMG = 0;
    private static final int CODE_GALLERY_VIDEO = 1;
    private static final int CODE_CAMERA = 2;
    private View myView;
    private Bus mBus;
    private Contact contact;
    private ChatSession chatSession;
    private MyMsgViewAdapter myMsgViewAdapter;
    private ArrayList<Envelop> chatHistory = new ArrayList<Envelop>();

    public ChatFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStop() {

        // Save chat history into a new chatsession and overwrite the original one
        super.onStop();
        DataController.getInstance().addChatSessions(getActivity().getApplicationContext(),new ChatSession(contact,chatHistory));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_chat, container, false);
        mBus = BusProvider.getInstance();
        mBus.register(this);
        assignActions();
        Bundle bundle = getArguments();
        String uid = bundle.getString("uid");

        final TextView chatObjectUidTV = (TextView) myView.findViewById(R.id.text_name);
        chatObjectUidTV.setText(uid);

        DataController dcInstance = DataController.getInstance();
        contact = dcInstance.getContactById(getActivity().getApplicationContext(),uid);
        chatSession =dcInstance.getChatSessionById(getActivity().getApplicationContext(),uid);

        if (chatSession==null){
            ChatSession newChatSession = new ChatSession(contact);
            dcInstance.addChatSessions(getActivity().getApplicationContext(),newChatSession);
            chatSession = dcInstance.getChatSessionById(getActivity().getApplicationContext(),uid);
        }
        final ListView list_msg = (ListView)myView.findViewById(R.id.list_msg);

        chatHistory = chatSession.chatHistory;
        myMsgViewAdapter =new MyMsgViewAdapter(getActivity(), R.layout.mychatmessage_list_pattern,chatHistory);
        myMsgViewAdapter.notifyDataSetChanged();
        list_msg.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //--------------- IMG ------------------
                if (chatHistory.get(position).msg.msgType == ChatMsg.MsgType.IMAGE) {//image item onclick
                    try {
                        Bitmap image = StorageController.getInstance().loadImage(chatHistory.get(position).resourceLink);
                        ((ImageView)view.findViewById(R.id.myChatImage)).setImageBitmap(Bitmap.createScaledBitmap(image, 300, 300, false));
                        view.findViewById(R.id.myChatContentTV).setVisibility(View.GONE);
                        final java.util.Timer timer = new java.util.Timer(true);
                        final Envelop thisEnvelop = chatHistory.get(position);
                        thisEnvelop.open();
                        TimerTask task = new TimerTask() {
                            @Override
                            public void run() {
                                if (getActivity() != null) {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        deleteEnvelop(thisEnvelop);
                                        //myMsgViewAdapter.remove(thisEnvelop);
                                        Toast.makeText(getActivity().getApplicationContext(), "Expired", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                            }
                        };
                        timer.schedule(task,1000*thisEnvelop.expireSeconds);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    //--------------- VIDEO ------------------
                }else if (chatHistory.get(position).msg.msgType == ChatMsg.MsgType.VIDEO) {
                        //video item onclick
                        VideoView video=(VideoView) view.findViewById(R.id.myChatVideo);
                        MediaController mediaController = new MediaController(getActivity());
                        mediaController.setAnchorView(video);
                        video.setMediaController(mediaController);
                        video.setKeepScreenOn(true);
                        video.setVideoURI(Uri.parse(chatHistory.get(position).resourceLink));

                        //video.setVideoPath(chatHistory.get(position).resourceLink);
                        //video.start();
                        video.requestFocus();
                        view.findViewById(R.id.myChatContentTV).setVisibility(View.GONE);
                        final java.util.Timer timer = new java.util.Timer(true);
                        final Envelop thisEnvelop = chatHistory.get(position);
                        thisEnvelop.open();
                        TimerTask task = new TimerTask() {
                            @Override
                            public void run() {
                                if (getActivity() != null) {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            deleteEnvelop(thisEnvelop);
                                            Toast.makeText(getActivity().getApplicationContext(), "Expired", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }
                        };
                        timer.schedule(task,1000*thisEnvelop.expireSeconds);
                }
            }
        });
        list_msg.setAdapter(myMsgViewAdapter);



        return myView;
    }

    private class MyMsgViewAdapter extends ArrayAdapter<Envelop> {
        private int layout;

        public MyMsgViewAdapter(Context context, int resource, List<Envelop> objects) {
            super(context, resource, objects);
            layout = resource;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final java.util.Timer timer = new java.util.Timer(true);
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(layout, parent, false);
            final ViewHolder viewHolder = new ViewHolder();
            //-------------------------- TEXT --------------------------
            if(getItem(position).msg.msgType == ChatMsg.MsgType.TEXT){
                viewHolder.nickname = (TextView) convertView.findViewById(R.id.myChatNickNameTV);
                viewHolder.content = (TextView) convertView.findViewById(R.id.myChatContentTV);
                viewHolder.video = (VideoView) convertView.findViewById(R.id.myChatVideo);
                viewHolder.image = (ImageView) convertView.findViewById(R.id.myChatImage);
                viewHolder.video.setVisibility(View.GONE);
                viewHolder.image.setVisibility(View.GONE);
                if (getItem(position).status == Envelop.EnvelopStatus.SENT){
                viewHolder.nickname.setText(getItem(position).from.nick);
                viewHolder.content.setText(getItem(position).msg.msgStr);
                }else if(getItem(position).status == Envelop.EnvelopStatus.OPENED){
                    Long lastTime = -Calendar.getInstance().getTimeInMillis() + (getItem(position).openTime.getTime())+1000*getItem(position).expireSeconds;                if ( lastTime>0) {
                    viewHolder.nickname.setText(getItem(position).from.nick);
                    viewHolder.content.setText(getItem(position).msg.msgStr);
                    final Envelop thisEnvelop = getItem(position);
                    TimerTask task = new TimerTask() {
                        @Override
                        public void run() {
                            if (getActivity() != null) {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        deleteEnvelop(thisEnvelop);
                                        //myMsgViewAdapter.remove(thisEnvelop);
                                        Toast.makeText(getActivity().getApplicationContext(), "Expired", Toast.LENGTH_SHORT).show();//SHOWN BUT NOT UPDATE

                                    }
                                });
                            }
                        }
                    };
                    timer.schedule(task, lastTime);
                } else {
                    deleteEnvelop(getItem(position));
                    Toast.makeText(getActivity().getApplicationContext(), "Expired", Toast.LENGTH_SHORT).show();
                }
            }else if(getItem(position).status == Envelop.EnvelopStatus.DELIVERED){
                getItem(position).open();
                viewHolder.nickname.setText(getItem(position).from.nick);
                viewHolder.content.setText(getItem(position).msg.msgStr);
                final Envelop thisEnvelop = getItem(position);
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    deleteEnvelop(thisEnvelop);
                                    Toast.makeText(getActivity().getApplicationContext(), "Expired", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    }
                };
                timer.schedule(task,1000*getItem(position).expireSeconds);
            }
                //-------------------------- IMG --------------------------
            } else if(getItem(position).msg.msgType == ChatMsg.MsgType.IMAGE){
                viewHolder.nickname = (TextView) convertView.findViewById(R.id.myChatNickNameTV);
                viewHolder.content = (TextView) convertView.findViewById(R.id.myChatContentTV);
                viewHolder.video = (VideoView) convertView.findViewById(R.id.myChatVideo);
                viewHolder.image = (ImageView) convertView.findViewById(R.id.myChatImage);
                viewHolder.video.setVisibility(View.GONE);
                if (getItem(position).status == Envelop.EnvelopStatus.SENT){
                    viewHolder.nickname.setText(getItem(position).from.nick);
                    viewHolder.content.setVisibility(View.GONE);
                    try {
                        Bitmap sentImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), Uri.parse(getItem(position).resourceLink));
                        //Bitmap sentImage = StorageController.getInstance().loadImage(getItem(position).resourceLink);
                        viewHolder.image.setImageBitmap(Bitmap.createScaledBitmap(sentImage, 300, 300, false));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else if(getItem(position).status == Envelop.EnvelopStatus.OPENED){
                    Long lastTime = -Calendar.getInstance().getTimeInMillis() + (getItem(position).openTime.getTime())+1000*getItem(position).expireSeconds;
                    if ( lastTime>0) {
                        viewHolder.nickname.setText(getItem(position).from.nick);
                        viewHolder.content.setVisibility(View.GONE);
                        try {
                            Bitmap sentImage = StorageController.getInstance().loadImage(getItem(position).resourceLink);
                            viewHolder.image.setImageBitmap(Bitmap.createScaledBitmap(sentImage, 300, 300, false));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        final Envelop thisEnvelop = getItem(position);
                        TimerTask task = new TimerTask() {
                            @Override
                            public void run() {
                                if (getActivity() != null) {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            deleteEnvelop(thisEnvelop);
                                            //myMsgViewAdapter.remove(thisEnvelop);
                                            Toast.makeText(getActivity().getApplicationContext(), "Expired", Toast.LENGTH_SHORT).show();//SHOWN BUT NOT UPDATE
                                            //deleteEnvelop(thisEnvelop);
                                            //myMsgViewAdapter.remove(getItem(position));
                                        }
                                    });
                                }
                            }
                        };
                        timer.schedule(task, lastTime);
                    } else {
                        deleteEnvelop(getItem(position));
                       // myMsgViewAdapter.remove(getItem(position));
                        Toast.makeText(getActivity().getApplicationContext(), "Expired", Toast.LENGTH_SHORT).show();
                    }
                }else if(getItem(position).status == Envelop.EnvelopStatus.DELIVERED){
                    viewHolder.nickname.setText(getItem(position).from.nick);
                    viewHolder.content.setText("Click to view the image");
                }
                //-------------------------- VIDEO --------------------------
            }else if(getItem(position).msg.msgType == ChatMsg.MsgType.VIDEO){
                //video getView
                viewHolder.nickname = (TextView) convertView.findViewById(R.id.myChatNickNameTV);
                viewHolder.content = (TextView) convertView.findViewById(R.id.myChatContentTV);
                viewHolder.video = (VideoView) convertView.findViewById(R.id.myChatVideo);
                viewHolder.image = (ImageView)convertView.findViewById(R.id.myChatImage);
                viewHolder.image.setVisibility(View.GONE);
                if (getItem(position).status == Envelop.EnvelopStatus.SENT){
                    //video sent
                    viewHolder.nickname.setText(getItem(position).from.nick);
                    viewHolder.content.setVisibility(View.GONE);
                    MediaController mediaController = new MediaController(getActivity());
                    mediaController.setAnchorView( viewHolder.video);
                    viewHolder.video.setMediaController(mediaController);
                    viewHolder.video.setKeepScreenOn(true);
                    viewHolder.video.setVideoPath(getItem(position).resourceLink);// It is working!
                    //viewHolder.video.setVideoURI( Uri.parse(getItem(position).resourceLink));
                    //viewHolder.video.start();
                    //viewHolder.video.requestFocus();
                }else if(getItem(position).status == Envelop.EnvelopStatus.OPENED){
                    //video opened
                    Long lastTime = -Calendar.getInstance().getTimeInMillis() + getItem(position).openTime.getTime()+1000*getItem(position).expireSeconds;
                    if ( lastTime>0) {
                        viewHolder.nickname.setText(getItem(position).from.nick);
                        viewHolder.content.setVisibility(View.GONE);
                        MediaController mediaController = new MediaController(getActivity());
                        mediaController.setAnchorView( viewHolder.video);
                        viewHolder.video.setMediaController(mediaController);
                        viewHolder.video.setKeepScreenOn(true);
                        File f = new File(getItem(position).resourceLink);
                        f.setReadable(true,false);
                        viewHolder.video.setVideoURI(Uri.parse(getItem(position).resourceLink));
                        //viewHolder.video.start();
                        viewHolder.video.requestFocus();
                        final Envelop thisEnvelop = getItem(position);
                        TimerTask task = new TimerTask() {
                            @Override
                            public void run() {
                                if (getActivity() != null) {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            deleteEnvelop(thisEnvelop);
                                            Toast.makeText(getActivity().getApplicationContext(), "Expired", Toast.LENGTH_SHORT).show();//SHOWN BUT NOT UPDATE
                                        }
                                    });
                                }
                            }
                        };
                        timer.schedule(task, lastTime);
                    } else {
                        deleteEnvelop(getItem(position));
                        Toast.makeText(getActivity().getApplicationContext(), "Expired", Toast.LENGTH_SHORT).show();
                    }
                }else if(getItem(position).status == Envelop.EnvelopStatus.DELIVERED){
                    //video delivered
                    viewHolder.nickname.setText(getItem(position).from.nick);
                    viewHolder.content.setText("Click to play the video");
                }
            }
            /*
            else if(getItem(position).msg.msgType == ChatMsg.MsgType.WITHDRAW){
                Contact from = getItem(position).from;
                deleteEnvelop(getItem(position));
                for (int i = chatHistory.size()-1;i>=0;i--) {
                    if (getItem(i).status != Envelop.EnvelopStatus.SENT) {
                        deleteEnvelop(getItem(i));
                        Toast.makeText(getActivity().getApplicationContext(), "Message Withdrawn Received", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }

            }
            */

            return convertView;
        }
    }

    public synchronized void deleteEnvelop(Envelop envelop){
        myMsgViewAdapter.remove(envelop);
    }
    public synchronized void addEnvelop(Envelop envelop){
        myMsgViewAdapter.add(envelop);
    }

    public class ViewHolder {
        TextView nickname;
        TextView content;
        ImageView image;
        VideoView video;
    }

    private void assignActions() {
        ImageView imgViewExit = (ImageView) myView.findViewById(R.id.imgview_chat_exit);
        //Back to session list
        imgViewExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EmptyFragment sessionListFragment = new EmptyFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(
                        R.id.layout_content_main,
                        sessionListFragment
                ).commit();

                ViewPager pager = (ViewPager) getActivity().findViewById(R.id.pager);
                pager.setVisibility(View.VISIBLE);
                pager.setAdapter( new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager()) );

            }
        });

        ImageView imgViewOption = (ImageView) myView.findViewById(R.id.imgview_chat_option);
        imgViewOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Edit Contact", Toast.LENGTH_LONG).show();
            }
        });

        //When press enter, send msg
        final EditText editTextSendMsg = (EditText) myView.findViewById(R.id.edit_chat_msg);
        editTextSendMsg.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    String msgStr = editTextSendMsg.getText().toString();
                    Contact from = DataController.getInstance().getUser(getActivity().getApplicationContext());
                    Contact to = contact;

                    MessageController.getInstance().sendTextMsg(sendCallback, getActivity().getApplicationContext(), from, to, msgStr, 20);
                    editTextSendMsg.setText("");

                    return true;
                }
                return false;
            }
        });
        //send msg
        final Button btnSend = (Button) myView.findViewById(R.id.btn_send);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String msgStr = editTextSendMsg.getText().toString();
                Contact from = DataController.getInstance().getUser(getActivity().getApplicationContext());
                Contact to = contact;

                MessageController.getInstance().sendTextMsg(sendCallback, getActivity().getApplicationContext(), from, to, msgStr, 20);
                editTextSendMsg.setText("");
            }
        });

        //withdraw msg
        final Button btnWithdraw = (Button) myView.findViewById(R.id.btn_withdraw);
        btnWithdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Contact from = DataController.getInstance().getUser(getActivity().getApplicationContext());
                Contact to = contact;
                //delete latest send message
                Date now = new Date();
                for (int i = chatHistory.size()-1;i>=0;i--) {
                    if ((chatHistory.get(i).from.uid.equals(from.uid))&& (chatHistory.get(i).status == Envelop.EnvelopStatus.SENT)) {
                        if (now.getTime() - chatHistory.get(i).createTime.getTime() < 300000) {//5 mins
                            MessageController.getInstance().sendWithdrawMsg(new MyVolley.VolleySendCallback() {
                                @Override
                                public void onSentSuccess(ServerResponse result, Envelop envelop) {
                                    Toast.makeText(getActivity().getApplicationContext(), "Message recalled", Toast.LENGTH_SHORT).show();
                                }
                            }, getActivity().getApplicationContext(), from, to);
                            deleteEnvelop(chatHistory.get(i));
                            break;
                        } else {
                            Toast.makeText(getActivity().getApplicationContext(), "Failed recall message", Toast.LENGTH_SHORT).show();
                            break;
                        }
                    }
                }
            }
        });


        final Button btnImg = (Button) myView.findViewById(R.id.btn_image);
        btnImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent imageActionIntent = new Intent(Intent.ACTION_GET_CONTENT);
                imageActionIntent.setType("image/*");
                startActivityForResult(imageActionIntent, CODE_GALLERY_IMG);
            }
        });


        final Button btnVideo = (Button) myView.findViewById(R.id.btn_video);
        btnVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent videoActionIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Intent videoActionIntent = new Intent(Intent.ACTION_GET_CONTENT);
                videoActionIntent.setType("video/*");
                startActivityForResult(videoActionIntent, CODE_GALLERY_VIDEO);
            }
        });

        final ImageView imgCamera = (ImageView) myView.findViewById(R.id.imgview_chat_camera);
        imgCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent cameraActionIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraActionIntent, CODE_CAMERA);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }
        Toast.makeText(getActivity().getApplicationContext(),"Uploading...", Toast.LENGTH_LONG).show();
        switch (requestCode) {
            case CODE_CAMERA:
                if (resultCode == Activity.RESULT_OK){
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    ContentValues values = new ContentValues();
                    values.put(MediaStore.Images.Media.TITLE, "title");
                    values.put(MediaStore.Images.Media.BUCKET_ID, "test");
                    values.put(MediaStore.Images.Media.DESCRIPTION, "test Image taken");
                    values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                    Uri uri = getActivity().getApplicationContext().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                    OutputStream outstream;
                    try {
                        outstream = getActivity().getApplicationContext().getContentResolver().openOutputStream(uri);
                        photo.compress(Bitmap.CompressFormat.JPEG, 70, outstream);
                        outstream.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.d(TAG, uri.toString());
                    sendImg(uri);
                }
                break;
            case CODE_GALLERY_IMG:
                Uri imgUri = data.getData();
                if (imgUri != null) {
                    Log.d(TAG, data.getData().toString());
                    sendImg(imgUri);
                }
                break;
            case CODE_GALLERY_VIDEO:
                Uri vUri = data.getData();
                if (vUri != null) {
                    Log.d(TAG, data.getData().toString());
                    sendVideo(vUri);
                }
                break;
            default:
                break;
        }
    }



    MyVolley.VolleySendCallback sendCallback = new MyVolley.VolleySendCallback() {
        @Override
        public void onSentSuccess(ServerResponse result, Envelop envelop) {
            if(result.success) {
                addEnvelop(envelop);
                if (getActivity() != null) {
                    Toast.makeText(getActivity().getApplicationContext(), "Sent", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    private void sendImg(Uri imgUri) {
        try {
            Bitmap img = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imgUri);
            MessageController.getInstance().sendImgMsg(
                    sendCallback,
                    getActivity().getApplicationContext(),
                    DataController.getInstance().getUser(getActivity().getApplicationContext()),
                    contact,
                    img,
                    20,imgUri.toString());

        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void sendVideo(Uri vUri) {
        try {
            String path = getRealPathFromURI(getActivity().getApplicationContext(),vUri);
            Log.d(TAG, path);
            MessageController.getInstance().sendVideoMsg(
                    sendCallback,
                    getActivity().getApplicationContext(),
                    DataController.getInstance().getUser(getActivity().getApplicationContext()),
                    contact,
                    path,
                    20,
                    path);

        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Video.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Subscribe
    public void getNewMessage(final Envelop envelop) {
        Log.d(TAG, envelop.resourceLink);
        if (!envelop.from.uid.equals(contact.uid)) {
            return;
        }
        if (getActivity() != null) {

            if (envelop.msg.msgType == ChatMsg.MsgType.WITHDRAW) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = chatHistory.size() - 1; i >= 0; i--) {
                            if ((envelop.from.uid.equals(chatHistory.get(i).from.uid)) && (chatHistory.get(i).status != Envelop.EnvelopStatus.SENT)) {
                                deleteEnvelop(chatHistory.get(i));
                                Toast.makeText(getActivity().getApplicationContext(), "1 Message has been Withdrawn", Toast.LENGTH_SHORT).show();
                                break;
                            }
                        }
                    }
                });
            } else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        addEnvelop(envelop);
                        Toast.makeText(getActivity().getApplicationContext(), "Message Received", Toast.LENGTH_SHORT).show();// not shown
                        Log.d("Get message", envelop.resourceLink);
                    }
                });
            }
        }
    }
}
