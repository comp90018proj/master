package com.uom.cryz.snapmessenger.controllers;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.uom.cryz.snapmessenger.fbservices.MyFirebaseStorageManager;
import com.uom.cryz.snapmessenger.models.Contact;
import com.uom.cryz.snapmessenger.models.Envelop;
import com.uom.cryz.snapmessenger.models.messages.ChatMsg;
import com.uom.cryz.snapmessenger.models.responses.ServerResponse;
import com.uom.cryz.snapmessenger.util.NetworkChecker;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by ruijiankun on 2/10/2016.
 */
public class MessageController {
    private static final String MSG_SEND_URL = "http://jackrui.uicp.hk/comp90018/messaging.php";
    private static final String TAG = MessageController.class.getSimpleName();
    private static MessageController msgController = null;

    public static MessageController getInstance() {
        if (msgController == null) {
            msgController = new MessageController();
        }
        return msgController;
    }

    private StorageReference storeReference;

    public MessageController() {
        storeReference = FirebaseStorage.getInstance().getReferenceFromUrl(MyFirebaseStorageManager.STORAGE_PATH);
    }

    private void sendMsg(final MyVolley.VolleySendCallback callback, Context context, final Envelop envelop) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, MSG_SEND_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        ServerResponse resp = new Gson().fromJson(response, ServerResponse.class);
                        callback.onSentSuccess(resp, envelop);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle error
                        Log.d(TAG, error.getStackTrace().toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("from", envelop.from.uid);
                headers.put("to", envelop.to.uid);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return envelop.msg.toJsonString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", envelop.msg.toJsonString(), "utf-8");
                    return null;
                }
            }
        };
        MyVolley.getInstance(context).addToRequestQueue(stringRequest);
    }

    public void sendTextMsg(final MyVolley.VolleySendCallback callback, Context context, Contact from, Contact to, String msgStr, long lastingTime) {
        if (!NetworkChecker.isNetworkOK(context)) {
            Toast.makeText(context, "Network Error!", Toast.LENGTH_SHORT).show();
            callback.onSentSuccess(new ServerResponse(false), null);
            return;
        }
        final String sender = from.uid;
        final String receiver = to.uid;
        final ChatMsg msg = new ChatMsg(sender, receiver, ChatMsg.MsgType.TEXT, msgStr, lastingTime);
        Envelop envelop = new Envelop(from, to, Envelop.EnvelopStatus.SENT, lastingTime, msg);
        sendMsg(callback, context, envelop);
    }


    public void sendImgMsg(final MyVolley.VolleySendCallback callback, final Context context, final Contact from, final Contact to, Bitmap img, final long lastingTime, final String resourceUri) {
        if (!NetworkChecker.isNetworkOK(context)) {
            Toast.makeText(context, "Network Error!", Toast.LENGTH_SHORT).show();
            callback.onSentSuccess(new ServerResponse(false), null);
            return;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        img.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        String fileName = String.format("%s_%s_%s_%d.jpg", from.uid, to.uid, new Date().getTime(), new Random().nextInt(100));
        MyFirebaseStorageManager.getInstance().uploadFile(
                new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        String msgStr = taskSnapshot.getDownloadUrl().toString();
                        ChatMsg msg = new ChatMsg(from.uid, to.uid, ChatMsg.MsgType.IMAGE, msgStr, lastingTime);
                        Envelop envelop = new Envelop(from, to, Envelop.EnvelopStatus.SENT, lastingTime, msg, resourceUri);
                        sendMsg(callback, context, envelop);
                    }
                },
                storeReference.child(MyFirebaseStorageManager.IMAGE_PATH + fileName),
                data
        );
    }


    public void sendVideoMsg(final MyVolley.VolleySendCallback callback, final Context context, final Contact from, final Contact to, String vUri, final long lastingTime, final String resourceUri) {
        if (!NetworkChecker.isNetworkOK(context)) {
            Toast.makeText(context, "Network Error!", Toast.LENGTH_SHORT).show();
            callback.onSentSuccess(new ServerResponse(false), null);
            return;
        }

        File f = new File(vUri);
        if (f.length() > 0 && f.length() < 10000000) {

            byte[] data = new byte[(int) f.length()];
            try {
                BufferedInputStream buf = new BufferedInputStream(new FileInputStream(f));
                buf.read(data, 0, data.length);
                buf.close();
            } catch (FileNotFoundException e) {
                Toast.makeText(context, "Read file error!", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            } catch (IOException e) {
                Toast.makeText(context, "Read file error!", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            String fileName = String.format("%s_%s_%s_%d.mp4", from.uid, to.uid, new Date().getTime(), new Random().nextInt(100));
            MyFirebaseStorageManager.getInstance().uploadFile(
                    new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            String msgStr = taskSnapshot.getDownloadUrl().toString();
                            ChatMsg msg = new ChatMsg(from.uid, to.uid, ChatMsg.MsgType.VIDEO, msgStr, lastingTime);
                            Envelop envelop = new Envelop(from, to, Envelop.EnvelopStatus.SENT, lastingTime, msg, resourceUri);
                            sendMsg(callback, context, envelop);
                        }
                    },
                    storeReference.child(MyFirebaseStorageManager.VIDEO_PATH + fileName),
                    data
            );
        } else {
            Toast.makeText(context, "Video file too large!", Toast.LENGTH_SHORT).show();
        }
    }

    public void sendWithdrawMsg(final MyVolley.VolleySendCallback callback, Context context, Contact from, Contact to) {
        if (!NetworkChecker.isNetworkOK(context)) {
            Toast.makeText(context, "Network Error!", Toast.LENGTH_SHORT).show();
            callback.onSentSuccess(new ServerResponse(false), null);
            return;
        }
        final String sender = from.uid;
        final String receiver = to.uid;
        final ChatMsg msg = new ChatMsg(sender, receiver, ChatMsg.MsgType.WITHDRAW, "");
        Envelop envelop = new Envelop(from, to, Envelop.EnvelopStatus.SENT, msg.msgLife, msg);
        sendMsg(callback, context, envelop);
    }
}
