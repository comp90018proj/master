package com.uom.cryz.snapmessenger.controllers;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.uom.cryz.snapmessenger.models.NearbyFriend;
import com.uom.cryz.snapmessenger.models.responses.FriendGetResponse;
import com.uom.cryz.snapmessenger.models.responses.NearbyFriendResponse;
import com.uom.cryz.snapmessenger.models.responses.ServerResponse;
import com.uom.cryz.snapmessenger.util.NetworkChecker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ruijiankun on 17/10/2016.
 */
public class GeoInfoController {

    private static final String GEO_UPDATE_URL = "http://jackrui.uicp.hk/comp90018/geo_add.php";
    private static final String GEO_GET_URL = "http://jackrui.uicp.hk/comp90018/geo_get.php";

    private static final String TAG = GeoInfoController.class.getSimpleName();

    private static GeoInfoController controller;

    LocationManager locationManager;


    public static GeoInfoController getInstance(Context context) {
        if (controller == null) {
            controller = new GeoInfoController(context);
        }
        return controller;
    }

    public boolean hasPermission = false;
    public Location lastLocation;
    LocationListener locationListener;

    public GeoInfoController(final Context context) {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        // Define a listener that responds to location updates
        trigger(context);
    }

    public void trigger(final Context context) {
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                lastLocation = location;
                updateGeoInfo(
                        new MyVolley.VolleyCallback() {
                            @Override
                            public void onSuccess(Object result) {
                                Log.d(TAG, String .format("Location-Lng:%f, Lat:%f",lastLocation.getLongitude(),lastLocation.getLatitude()));
                            }
                        },
                        context,
                        location);

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        // Register the listener with the Location Manager to receive location updates
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, locationListener);
    }

    public void getNearbyFriends(final MyVolley.VolleyCallback callback, final Context context) {
        if (!NetworkChecker.isNetworkOK(context)) {
            Toast.makeText(context, "Network Error!", Toast.LENGTH_SHORT).show();
            callback.onSuccess(new NearbyFriendResponse(false));
            return;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, GEO_GET_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        NearbyFriendResponse resp = new Gson().fromJson(response, NearbyFriendResponse.class);
                        callback.onSuccess(resp);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.getStackTrace().toString());
                    }
                });

        MyVolley.getInstance(context).addToRequestQueue(stringRequest);
    }

    public void updateGeoInfo(final MyVolley.VolleyCallback callback, final Context context, final Location location) {
        if (!NetworkChecker.isNetworkOK(context)) {
            Toast.makeText(context, "Network Error!", Toast.LENGTH_SHORT).show();
            callback.onSuccess(new ServerResponse(false));
            return;
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, GEO_UPDATE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        ServerResponse resp = new Gson().fromJson(response, ServerResponse.class);
                        callback.onSuccess(resp);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.getStackTrace().toString());
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("uid", DataController.getInstance().getUser(context).uid);
                headers.put("lng", Double.toString(location.getLongitude()));
                headers.put("lat", Double.toString(location.getLatitude()));
                return headers;
            }
        };

        MyVolley.getInstance(context).addToRequestQueue(stringRequest);
    }

    private static final int TEN_MINUTES = 1000 * 60 * 10;

    /** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TEN_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TEN_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

}
