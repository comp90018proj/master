package com.uom.cryz.snapmessenger;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.otto.Subscribe;
import com.uom.cryz.snapmessenger.controllers.DataController;
import com.uom.cryz.snapmessenger.models.ChatSession;
import com.uom.cryz.snapmessenger.models.Contact;
import com.uom.cryz.snapmessenger.models.Envelop;
import com.uom.cryz.snapmessenger.models.messages.ChatMsg;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SessionListFragment extends Fragment {

    public static final String TAG = SessionListFragment.class.getSimpleName();

    public SessionListFragment() {
        // Required empty public constructor
    }

    private View myView;
    private ArrayList<ChatSession> chatSessions;
    private  MySessionListViewAdapter mySessionListViewAdapter;

    private class MySessionListViewAdapter extends ArrayAdapter<ChatSession> {
        private int layout;

        public MySessionListViewAdapter(Context context, int resource, List<ChatSession> objects) {
            super(context, resource, objects);
            layout = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //final Contact receiveContact = getItem(position);
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(layout, parent, false);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.nickname = (TextView) convertView.findViewById(R.id.sessionUserNickNameTV);
            viewHolder.msg = (TextView) convertView.findViewById(R.id.sessionLastUpdateMsgTV);
            viewHolder.nickname.setText(getItem(position).contact.nick);
            viewHolder.msg.setText("");
            //ChatSession chatSession= DataController.getInstance().getChatSessionById(getActivity().getApplicationContext(),getItem(position).contact.uid);
            /*Date last = null;
            Envelop lastEnvelop = null;
            if (chatSession.chatHistory!=null){
                for(Envelop envelop: chatSession.chatHistory){
                    //find the latest envelop based on create time
                    if (last == null){
                        last = envelop.createTime;
                        lastEnvelop = envelop;
                    }else{
                        if (last.before(envelop.createTime)){
                            last=envelop.createTime;
                            lastEnvelop = envelop;
                        }
                    }
                }
                viewHolder.msg.setText(lastEnvelop.createTime.toString());
            }else{
                viewHolder.msg.setText("");
            }
            */
            return convertView;
        }
    }

    public class ViewHolder {
        TextView nickname;
        TextView msg;
        //Button addMeAddBtn;
    }
/* TODO May not be useful. Depends on Alvin's fix on fragment replacement.
    @Override
    public void onResume() {
        super.onResume();
        chatSessions = DataController.getInstance().getChatSessions(getActivity().getApplicationContext());
    }
*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_session_list, container, false);
        assignActions();
        chatSessions = DataController.getInstance().getChatSessions(getActivity().getApplicationContext());
        ListView list_session = (ListView) myView.findViewById(R.id.list_session);
        list_session.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String opponentUid = chatSessions.get(position).contact.uid;
                Bundle bundle = new Bundle();
                bundle.putString("uid", opponentUid);
                FragmentTransaction ft = getActivity().getSupportFragmentManager()
                        .beginTransaction();
                ChatFragment chatFragment = new ChatFragment();
                chatFragment.setArguments(bundle);
                ft.replace(R.id.layout_content_main, chatFragment, chatFragment.getTag());
                ft.commit();
                ViewPager pager = (ViewPager)getActivity().findViewById(R.id.pager);
                pager.setVisibility(View.GONE);
            }
        });
        mySessionListViewAdapter = new MySessionListViewAdapter(getActivity(), R.layout.session_listview_pattern, chatSessions);
        mySessionListViewAdapter.setNotifyOnChange(true);
        list_session.setAdapter(mySessionListViewAdapter);

        return myView;
    }

    private void assignActions() {
        ImageView imgViewCamera = (ImageView) myView.findViewById(R.id.imgview_camera);
        imgViewCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: back to camera

                ViewPager pager = (ViewPager)getActivity().findViewById(R.id.pager);
                pager.setCurrentItem(1);
                ViewPager vpager = (ViewPager)getActivity().findViewById(R.id.vpager);
                vpager.setCurrentItem(1);
            }
        });


        ImageView imgViewNewChat = (ImageView) myView.findViewById(R.id.imgview_add_chat);
        imgViewNewChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: show contact
                Fragment myFriendsFragment  = new MyFriendsFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                Bundle b = new Bundle();
                b.putString("fromPage", TAG);
                myFriendsFragment.setArguments(b);
                fm.beginTransaction().replace(
                        R.id.layout_content_main,
                        myFriendsFragment,
                        myFriendsFragment.getTag()
                ).commit();
                ViewPager pager = (ViewPager)getActivity().findViewById(R.id.pager);
                pager.setVisibility(View.GONE);
            }
        });

    }

    @Subscribe
    public void getNewMessage(final Envelop envelop) {

    }
}
