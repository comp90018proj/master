package com.uom.cryz.snapmessenger;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CameraFragment extends Fragment implements SurfaceHolder.Callback {
    Button btnTakePhoto;
    Button btnSwitch;
    Button btnSavePhoto;
    Button btnSend;
    Button btnRecording;
    Button btnBack;
    Button btnEdit;

    Bitmap photo;
    SurfaceHolder surfaceHolder;
    SurfaceView surfaceView;
    ImageView imageView;
    Camera.Parameters parameters;

    private int cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
    private static final String TAG = "tag";
    private Camera camera ;
    private MediaRecorder mMediaRecorder;
    private boolean isRecording = false;
    private View v;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_camera, container, false);
        surfaceView = (SurfaceView) v.findViewById(R.id.cameraSurface);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        initialize();
        return v;
    }

    public void initialize() {

        btnTakePhoto = (Button) v.findViewById(R.id.btnButton);
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePhoto();
            }
        });

        btnBack = (Button) v.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSend = (Button) v.findViewById(R.id.btnSend);
                btnBack = (Button) v.findViewById(R.id.btnBack);
                btnEdit = (Button) v.findViewById(R.id.btnEdit);
                camera.startPreview();
                getActivity().findViewById(R.id.cameraImage).setVisibility(View.INVISIBLE);
                btnSavePhoto.setVisibility(View.INVISIBLE);
                btnTakePhoto.setVisibility(View.VISIBLE);
                btnRecording.setVisibility(View.VISIBLE);
                btnSwitch.setVisibility(View.VISIBLE);
                btnSend.setVisibility(View.INVISIBLE);
                btnBack.setVisibility(View.INVISIBLE);
                btnEdit.setVisibility(View.INVISIBLE);
            }
        });

        btnEdit = (Button) v.findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editPicture();
            }
        });



        btnRecording = (Button) v.findViewById(R.id.button_capture);
        btnRecording.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isRecording) {
                            // stop recording and release camera
                            mMediaRecorder.stop();  // stop the recording
                            releaseMediaRecorder(); // release the MediaRecorder object
                            camera.lock();         // take camera access back from MediaRecorder



                            // inform the user that recording has stopped
                            btnRecording.setText("Video");
                            isRecording = false;

                        } else {
                            // initialize video camera
                            if (prepareVideoRecorder()) {
                                // Camera is available and unlocked, MediaRecorder is prepared,
                                // now you can start recording
                                mMediaRecorder.start();

                                // inform the user that recording has started
                                btnRecording.setText("Stop");
                                isRecording = true;
                            } else {
                                // prepare didn't work, release the camera
                                releaseMediaRecorder();
                                // inform user
                            }
                        }
                    }
                }
        );

        btnSwitch = (Button) v.findViewById(R.id.btnSwitch);
        btnSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchCamera();
            }
        });

        btnSavePhoto = (Button) v.findViewById(R.id.btnSave);
        btnSavePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadImagePublic();
            }
        });
    }


    public static Bitmap RotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public void takePhoto() {
        camera.takePicture(new Camera.ShutterCallback() {
            @Override
            public void onShutter() {

            }
        }, null, null, new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] bytes, Camera camera) {
                btnSend = (Button) v.findViewById(R.id.btnSend);
                btnBack = (Button) v.findViewById(R.id.btnBack);
                btnEdit = (Button) v.findViewById(R.id.btnEdit);
                photo = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                camera.stopPreview();
                photo = RotateBitmap(photo,90);
                btnSavePhoto.setVisibility(View.VISIBLE);
                btnTakePhoto.setVisibility(View.INVISIBLE);
                btnRecording.setVisibility(View.INVISIBLE);
                btnSwitch.setVisibility(View.INVISIBLE);
                btnSend.setVisibility(View.VISIBLE);
                btnBack.setVisibility(View.VISIBLE);
                btnEdit.setVisibility(View.VISIBLE);
                imageView = (ImageView) getActivity().findViewById(R.id.cameraImage);
                imageView.setImageBitmap(photo);
            }
        });
    }




    private String saveFile(Bitmap bitmap)
    {
        try
        {
            File file = File.createTempFile("img","");
            FileOutputStream fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);

            fos.flush();
            fos.close();
            return file.getAbsolutePath();

        }catch(IOException e)
        {
            e.printStackTrace();
        }

        return "";
    }

    public void editPicture(){
        imageView = (ImageView) getActivity().findViewById(R.id.cameraImage);
        Bitmap bitmap= photo;
        Bitmap mutableBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas=new Canvas(mutableBitmap);
        Paint p=new Paint();
        p.setColor(0xffff0000);
        p.setAntiAlias(true);
        p.setTextSize(25);
        canvas.drawText("text",10,20,p);

//        Intent it = new Intent(getActivity(), PreviewActivity.class);
        photo = mutableBitmap;
//        String path = saveFile(photo);
//        it.putExtra("path", path);
//        startActivity(it);
        imageView.setImageBitmap(photo);
    }


    public void switchCamera() {
        camera.stopPreview();
        if (camera!=null){
            camera.release();
            camera=null;
        }
        if(cameraId==Camera.CameraInfo.CAMERA_FACING_FRONT)
        {
            camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
            cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
            Toast.makeText(this.getActivity().getApplicationContext(),"CAMERA_FACING_BACK",Toast.LENGTH_LONG).show();
            camera.setDisplayOrientation(90);
        }
        else
        {
            camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
            cameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
            Toast.makeText(this.getActivity().getApplicationContext(),"CAMERA_FACING_FRONT",Toast.LENGTH_LONG).show();
            camera.setDisplayOrientation(90);
        }
        try {
            camera.setPreviewDisplay(surfaceHolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        camera.startPreview();
    }



    public void downloadImagePublic() {
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"MyCameraApp");
        if (!dir.exists()){
            dir.mkdir();
        }
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = new File(dir.getPath()+File.separator+"IMG_"+timestamp+".jpg");
        System.out.println(dir.getPath()+File.separator+"IMG_"+timestamp+".jpg");
        FileOutputStream outputStream =null;
        try {
            outputStream = new FileOutputStream(mediaFile);
            photo.compress(Bitmap.CompressFormat.JPEG,100,outputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }finally {
            if (outputStream!=null){
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
        this.getActivity().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(mediaFile)));
        Toast.makeText(this.getActivity().getApplicationContext(),"download successfully",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPause() {
        super.onPause();
        releaseMediaRecorder();       // if you are using MediaRecorder, release it first
        releaseCamera();
//        camera.release();
    }

    private void releaseMediaRecorder(){
        if (mMediaRecorder != null) {
            mMediaRecorder.reset();   // clear recorder configuration
            mMediaRecorder.release(); // release the recorder object
            mMediaRecorder = null;
            camera.lock();           // lock camera for later use
        }
    }


    private void releaseCamera(){
        if (camera != null){
            camera.release();
            camera = null;
        }
    }





    private boolean prepareVideoRecorder(){
        mMediaRecorder = new MediaRecorder();
//        camera= getCameraInstance();
        // Step 1: Unlock and set camera to MediaRecorder
        camera.unlock();
        mMediaRecorder.setCamera(camera);

        // Step 2: Set sources
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);

        // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
        mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));

        // Step 4: Set output file/System.out.println(getOutputMediaPath());
        mMediaRecorder.setOutputFile(getOutputMediaPath());

        // Step 5: Set the preview output
        mMediaRecorder.setPreviewDisplay(surfaceView.getHolder().getSurface());

        // Step 6: Prepare configured MediaRecorder
        try {
            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
            Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }

        return true;
    }



    private String getOutputMediaPath() {
        java.util.Date date = new java.util.Date();
        String timeTemp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date.getTime());
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        Log.e("Error", "getOutputMediaPath file path---"+
                mediaStorageDir.getPath() + File.separator +
                "VID_" + timeTemp+ ".mp4");
        return mediaStorageDir.getPath() + File.separator +
                "VID_" + timeTemp + ".mp4";
    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        while(true) {
            camera = getCameraInstance(cameraId);
            if(camera != null){
                break;
            }
            else{
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        parameters = camera.getParameters();
        camera.setParameters(parameters);
        camera.setDisplayOrientation(90);
        try {
            camera.setPreviewDisplay(surfaceHolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        camera.startPreview();
    }

    public static Camera getCameraInstance(int CameraID){
        Camera c = null;
        try {
            c = Camera.open(CameraID); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        // empty. Take care of releasing the Camera preview in your activity.

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        if (surfaceHolder.getSurface() == null){
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            camera.stopPreview();
        } catch (Exception e){
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings
        try {
            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();

        } catch (Exception e){
            Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }
    }

}
