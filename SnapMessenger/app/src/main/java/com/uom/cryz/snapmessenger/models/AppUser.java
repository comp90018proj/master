package com.uom.cryz.snapmessenger.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.iid.FirebaseInstanceId;
import com.uom.cryz.snapmessenger.MainActivity;

/**
 * Created by ruijiankun on 4/10/2016.
 */
public class AppUser extends Contact {

    public boolean loginStatus = false;

    public AppUser(String uid, String email, String nickName, String mobile, boolean login) {
        super(uid, email, nickName, "", mobile, FirebaseInstanceId.getInstance().getToken());
        this.loginStatus = login;
    }
}
