<?php
include 'config.php';
include 'sendmsg.php';

$uid='';
$email='';
$pwd='';
$token='';

foreach (getallheaders() as $name => $value) {
    switch ($name) {
        case "uid":
            $uid = $value;
            break;
        case "email":
            $email = $value;
            break;
        case "pwd":
            $pwd = $value;
            break;
        case "token":
            $token = $value;
            break;
        default:
            break;
    }
}

if(strlen($pwd) < 32 || strlen($token) == 0){
    echo $fail_json;
    exit(0);
}

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error){
    echo $fail_json;
    die("Connection failed: " . $conn->connect_error);
}

$query_sql = "SELECT * FROM users WHERE (uid='$uid' AND pwd='$pwd') OR (email='$email' AND pwd='$pwd')";
$result = $conn->query($query_sql);

if ($result->num_rows == 1) {
    $update_sql ="UPDATE users SET token='$token',login_flag=true WHERE (uid='$uid' AND pwd='$pwd') OR (email='$email' AND pwd='$pwd')";
    $conn->query($update_sql);
    
    // output data
    $row = $result->fetch_assoc();
    $row_uid = $row["uid"];
    $row_email = $row["email"];
    $row_nick = $row["nick"];
    $row_mobile = $row{"mobile"};
 
    $json = "{\"success\":true, \"uid\":\"$row_uid\", \"email\":\"$row_email\",\"nick\":\"$row_nick\",\"mobile\":\"$row_mobile\" }";
    echo $json;
    
    //TODO: send cached msg.
    $sel_sql = "SELECT * FROM msg_cache LEFT JOIN users ON msg_cache.receiver=users.uid WHERE receiver='$uid'";
    $cache = $conn->query(sel_sql);
    
    if ($cache->num_rows > 0) {
        //send data
        while($row = $cache->fetch_assoc()) {
            $row_uid = $row["receiver"];
            $row_token = $row["token"];
            $row_msg = $row["msg_content"];

            sendmessage($row_msg, $token);
        }
        //delete cache
        $del_sql = "DELETE FROM msg_cache WHERE receiver='$uid'";
        $conn->query(del_sql);
    }
    
    
} else {
    echo $fail_json;
}

$conn->close();
?>
