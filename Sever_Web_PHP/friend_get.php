<?php
include 'config.php';

$uid='';

foreach (getallheaders() as $name => $value) {
    switch ($name) {
        case "uid":
            $uid = $value;
            break;
        default:
            break;
    }
}

if(strlen($uid) == 0){
    echo $fail_json;
    exit(0);
}

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error){
    echo $fail_json;
    die("Connection failed: " . $conn->connect_error);
}

//1.contacts
$contact_json = "{\"success\":true, \"contacts\":[";

$select_sql = "SELECT user_relation.friend_uid,user_relation.friend_remark,users.nick,users.mobile,user_relation.relation_status FROM user_relation LEFT JOIN users on user_relation.friend_uid = users.uid WHERE user_relation.uid='$uid' AND relation_status<1";
$result = $conn->query($select_sql);
if ($result->num_rows > 0) {
    // output data
    while($row = $result->fetch_assoc()) {
        $row_uid = $row["friend_uid"];
        $row_remark = $row["friend_remark"];
        $row_nick = $row["nick"];
        $row_mobile = $row["mobile"];
        $row_block = $row["relation_status"];
        $contact_json = $contact_json . "{\"uid\":\"$row_uid\", \"remark\":\"$row_remark\",\"nick\":\"$row_nick\", \"mobile\":\"$row_mobile\", \"blocked\":$row_block },";
    }
}
//remove last comma
$contact_json = rtrim($contact_json, ",") . "], \"requests\":[";
//2.friend request

$select_sql = "SELECT user_relation.uid,users.nick,users.mobile FROM user_relation LEFT JOIN users ON user_relation.uid = users.uid WHERE user_relation.friend_uid='$uid' AND relation_status=1;";
$result = $conn->query($select_sql);
if ($result->num_rows > 0) {
    // output data
    while($row = $result->fetch_assoc()) {
        $row_uid = $row["uid"];
        $row_nick = $row["nick"];
        $row_mobile = $row["mobile"];

        $contact_json = $contact_json . "{\"uid\":\"$row_uid\",\"nick\":\"$row_nick\", \"mobile\":\"$row_mobile\" },";
    }
}

$contact_json = rtrim($contact_json, ",") . "]}";
echo $contact_json;

$conn->close();
?>
