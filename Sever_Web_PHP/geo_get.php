<?php
include 'config.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error){
    echo $fail_json;
    die("Connection failed: " . $conn->connect_error);
}

//1.contacts
$contact_json = "{\"success\":true, \"contacts\":[";

$select_sql = "SELECT * FROM geo_info LEFT JOIN users ON geo_info.uid=users.uid";
$result = $conn->query($select_sql);
if ($result->num_rows > 0) {
    // output data
    while($row = $result->fetch_assoc()) {
        $row_uid = $row["uid"];
        $row_nick = $row["nick"];
        $row_mobile = $row["mobile"];
        $row_lng = $row["lng"];
        $row_lat = $row["lat"];
        $row_update = $row["last_update"];
        $contact_json = $contact_json . "{\"uid\":\"$row_uid\", \"nick\":\"$row_nick\", \"mobile\":\"$row_mobile\",\"lng\":$row_lng,\"lat\":$row_lat,\"lastUpdate\":\"$row_update\" },";
    }
}
//remove last comma
$contact_json = rtrim($contact_json, ",") . "]}";

echo $contact_json;

$conn->close();
?>
