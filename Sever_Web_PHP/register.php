<?php
include 'config.php';

$email='';
$pwd='';
$token='';
$nick='';
$mobile='';

foreach (getallheaders() as $name => $value) {
    switch ($name) {
        case "email":
            $email = $value;
            break;
        case "pwd":
            $pwd = $value;
            break;
        case "token":
            $token = $value;
            break;
        case "nick":
            $nick = $value;
            break;
        case "mobile":
            $mobile = $value;
            break;
        default:
            break;
    }
}

if(strlen($email) < 4 || strlen($pwd) < 32 || strlen($token) == 0){
    echo $fail_json;
    exit(0);
}


function generateRandomString($length = 3) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error){
    echo $fail_json;
    die("Connection failed: " . $conn->connect_error);
}

$sel_sql = "SELECT * FROM users WHERE email='$email'";
$result = $conn->query($sel_sql);
if($result->num_rows > 0){
    echo $fail_json;
    exit(0);
}


$exist = true;
$temp_uid = "";

while($exist){
    $temp_uid = strtolower(substr($email, 0, 4)) . generateRandomString();
    $select_sql = "SELECT * FROM users WHERE uid=temp_id";
    $result = $conn->query($select_sql);
    $exist = ($result->num_rows > 0);
}
    
$insert_sql = "INSERT INTO users (uid,email,pwd,nick,mobile,token,login_flag) VALUES ('$temp_uid','$email','$pwd','$nick','$mobile','$token',true)";

if ($conn->query($insert_sql) === TRUE) {
    echo "{\"success\":true, \"uid\":\"$temp_uid\", \"email\":\"$email\", \"nick\":\"$nick\", \"mobile\":\"$mobile\"}";
} else {
    echo $fail_json;
}

$conn->close();
?>
