<?php
include 'config.php';
include 'sendmsg.php';

$uid='';
$lng='';
$lat='';
$date = '';
foreach (getallheaders() as $name => $value) {
    switch ($name) {
        case "uid":
            $uid = $value;
            break;
        case "lng":
            $lng = $value;
            break;
        case "lat":
            $lat = $value;
            break;
        case "date":
            $date=$value;
        default:
            break;
    }
}

if(strlen($uid) == 0 || strlen($lng) == 0 || strlen($lat) == 0){
    echo $fail_json;
    exit(0);
}

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error){
    echo $fail_json;
    die("Connection failed: " . $conn->connect_error);
}

$sel_sql = "SELECT * FROM geo_info WHERE uid='$uid'";
$edit_sql = "INSERT INTO geo_info (uid,lng,lat,last_update) VALUES ('$uid', $lng, $lat, '$date')";
$result = $conn->query($sel_sql);
if ($result->num_rows > 0) {
    $edit_sql = "UPDATE geo_info SET lng=$lng,lat=$lat,last_update='$date' WHERE uid='$uid'";
}


if($conn->query($edit_sql) === TRUE) {
    echo $success_json;
} else {
    echo $fail_json;
}
$conn->close();
?>
