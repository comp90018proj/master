<?php
include 'config.php';

$uid='';
$friendId='';

foreach (getallheaders() as $name => $value) {
    switch ($name) {
        case "uid":
            $uid = $value;
            break;
        case "friendId":
            $friendId = $value;
            break;
        default:
            break;
    }
}

if(strlen($uid) == 0 || strlen($friendId) == 0){
    echo $fail_json;
    exit(0);
}

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error){
    echo $fail_json;
    die("Connection failed: " . $conn->connect_error);
}

$del_sql ="DELETE from user_relation WHERE (uid='$uid' AND friend_uid='$friendId') OR (friend_uid='$uid' AND uid='$friendId')";
if($conn->query($del_sql) === TRUE){
    echo $success_json;
}
else{
    echo $fail_json;
}
$conn->close();
?>
