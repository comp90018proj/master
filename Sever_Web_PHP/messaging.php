<?php
include 'config.php';
include 'sendmsg.php';

$from='';
$to='';

foreach (getallheaders() as $name => $value) {
    switch ($name) {
        case "from":
            $from = $value;
            break;
        case "to":
            $to = $value;
            break;
        default:
            break;
    }
}

if(strlen($from) == 0 || strlen($to) == 0){
    echo $fail_json;
    exit(0);
}

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error){
    echo $fail_json;
    die("Connection failed: " . $conn->connect_error);
}

//TODO: Send message
$sel_sql = "SELECT token FROM users WHERE uid='$to' AND login_flag=true";
$result = $conn->query($sel_sql);
if ($result->num_rows == 1) {
    // output data of each row
    $row = $result->fetch_assoc();
    $body = file_get_contents('php://input');
    $token = $row["token"];
    sendmessage($body , $token);
}else{
    $ins_sql = "INSERT INTO msg_cache (sender,receiver,send_time,msg_content) VALUES ('$from','$to',NOW(),'$body')";
    $conn->query($ins_sql);
}
echo $success_json;

$conn->close();

?>

