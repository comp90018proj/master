<?php
include 'config.php';

$uid='';

foreach (getallheaders() as $name => $value) {
    switch ($name) {
        case "uid":
            $uid = $value;
            break;
        default:
            break;
    }
}

if(strlen($uid) == 0){
    echo $fail_json;
    exit(0);
}

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error){
    echo $fail_json;
    die("Connection failed: " . $conn->connect_error);
}

$contact_json = "{\"success\":true, \"contact\":";

$select_sql = "SELECT * FROM users WHERE uid='$uid'";
$result = $conn->query($select_sql);
if ($result->num_rows > 0) {
    // output data
    $row = $result->fetch_assoc();
    $row_uid = $row["uid"];
    $row_email = $row["email"];
    $row_nick = $row["nick"];
    $row_mobile = $row["mobile"];
    $contact_json = $contact_json . "{\"uid\":\"$row_uid\", \"email\":\"$row_email\",\"nick\":\"$row_nick\", \"mobile\":\"$row_mobile\" }";
    
    $contact_json = $contact_json .  "}";
    echo $contact_json;

}else{
    echo $fail_json;
}


$conn->close();
?>
