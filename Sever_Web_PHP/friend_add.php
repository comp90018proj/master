<?php
include 'config.php';
include 'sendmsg.php';

$from='';
$to='';
$status='0';

foreach (getallheaders() as $name => $value) {
    switch ($name) {
        case "from":
            $from = $value;
            break;
        case "to":
            $to = $value;
            break;
        case "status":
            $status = $value;
            break;
        default:
            break;
    }
}

if(strlen($from) == 0 || strlen($to) == 0 || $status == '0'){
    echo $fail_json;
    exit(0);
}

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error){
    echo $fail_json;
    die("Connection failed: " . $conn->connect_error);
}

$sel_sql = "SELECT * FROM user_relation WHERE uid='$from' AND friend_uid='$to'";
$result = $conn->query($sel_sql);
if ($result->num_rows > 0) {
    echo $fail_json;
    $conn->close();
    exit(0);
}

if($from == $to)
{
    $ins_sql ="INSERT INTO user_relation (uid,friend_uid,friend_remark,relation_status) VALUES ('$from','$to','',0)";
    if($conn->query($ins_sql) === TRUE){
        echo $success_json;
    } else{
        echo $fail_json;
    }
    exit(0);
}

//request
if($status == '1'){
    $ins_sql ="INSERT INTO user_relation (uid,friend_uid,friend_remark,relation_status) VALUES ('$from','$to','',1)";
    if($conn->query($ins_sql) === TRUE){
        echo $success_json;
    } else{
        echo $fail_json;
    }
// accept    
} else if($status == '2') {
    $ins_sql = "INSERT INTO user_relation (uid,friend_uid,friend_remark,relation_status) VALUES ('$from','$to','',0)";   
    $update_sql = "UPDATE user_relation SET relation_status=0 WHERE uid='$to' AND friend_uid='$from'";
 
    if($conn->query($ins_sql) === TRUE && $conn->query($update_sql) === TRUE){
        echo $success_json;
    } else{
        echo $fail_json;
    }
} else if ($status == '3') {
    $del_sql = "DELETE FROM user_relation WHERE uid='$to' AND friend_uid='$from'";
    if($conn->query($del_sql) === TRUE){
        echo $success_json;
    } else{
        echo $fail_json;
    }
}

//TODO: Send message
$sel_sql = "SELECT token FROM users WHERE uid='$to' AND login_flag=true";
$result = $conn->query($sel_sql);
if ($result->num_rows == 1) {
    // output data of each row
    $row = $result->fetch_assoc();
    $token = $row["token"];
    $body = file_get_contents('php://input');
    sendmessage($body , $token);
}

$conn->close();
?>
