<?php
include 'config.php';
$uid='';
$friendId='';
$blockStatus=0;

foreach (getallheaders() as $name => $value) {
    switch ($name) {
        case "uid":
            $uid = $value;
            break;
        case "friendId":
            $friendId = $value;
            break;
        case "blockStatus":
            $blockStatus = $value;
            break;
        default:
            break;
    }
}

if(strlen($uid) == 0 || strlen($friendId) == 0 || $blockStatus > 0){
    echo $fail_json;
    exit(0);
}

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error){
    echo $fail_json;
    die("Connection failed: " . $conn->connect_error);
}

$update_sql ="UPDATE user_relation SET relation_status=$blockStatus WHERE uid='$uid' AND friend_uid='$friendId'";
if($conn->query($update_sql) === TRUE){
    echo $success_json;
}
else{
    echo $fail_json;
}
$conn->close();
?>
