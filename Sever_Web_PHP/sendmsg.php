<?php
function sendmessage($data, $target){
    $FB_API_KEY = 'AIzaSyCXxrQSve7DCWegf3-QDH3YfB-D9zh9ALo';
    $FB_SEND_URL = 'https://fcm.googleapis.com/fcm/send';
    $FB_CONTENT_TYPE = 'application/json';

    $content = "{\"to\":\"$target\",\"data\":{\"json\":$data},\"notification\":{\"title\":\"SnapMessenger\",\"body\":\"New Message Received.\"}}";
    
    //echo $content;
    //exit();
    //header with content_type api key
    $headers = array(
        'Content-Type:application/json',
        'Authorization:key='.$FB_API_KEY
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $FB_SEND_URL);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
    $result = curl_exec($ch);
    if ($result === FALSE) {
        die('FCM Send Error: ' . curl_error($ch));
    }
    curl_close($ch);
    return $result;
}

?>
