<?php
include 'config.php';

$uid='';
foreach (getallheaders() as $name => $value) {
    switch ($name) {
        case "uid":
            $uid = $value;
            break;
        default:
            break;
    }
}

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error){
    echo $fail_json;
    die("Connection failed: " . $conn->connect_error);
}

$update_sql ="UPDATE users SET token='',login_flag=false WHERE uid='$uid'";
if($conn->query($update_sql) === TRUE){
    echo $success_json;
}
else{
    echo $fail_json;
}
$conn->close();

?>
